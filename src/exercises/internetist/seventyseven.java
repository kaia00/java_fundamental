package exercises.internetist;

import java.lang.reflect.Array;
import java.util.Arrays;

public class seventyseven {
    public static void main(String[] args) {


        String result1 = "";
        int[] left_array = {1, 2, 3, -2};
        int[] right_array = {4, 5, 6, -7};
        System.out.println("Array1: "+Arrays.toString(left_array));
        System.out.println("Array2: "+Arrays.toString(right_array));
        for (int i = 0; i < left_array.length; i++) {
            int num1 = left_array[i];
            int num2 = right_array[i];

            result1 += Integer.toString(num1 * num2) + " ";
        }
        System.out.println("\nResult: "+result1);
    }
}



     /*   int[] myArray1 = new int[]{1, 2, 3};
        int[] myArray2 = new int[]{4, 5, 6};
        int[] newArray = new int[]{myArray1[0], myArray2[myArray2.length - 1]};
        System.out.println(Arrays.toString(myArray1));
        System.out.println(Arrays.toString(myArray2));
        System.out.println(Arrays.toString(newArray)); */







/*
77. Write a Java program to create a new array of length 2 from two arrays of integers with three elements and the new array will contain the first
 and last elements from the two arrays. Go to the editor
Test Data: array1 = 50, -20, 0
array2 = 5, -50, 10
Sample Output:

Array1: [50, -20, 0]
Array2: [5, -50, 10]
New Array: [50, 10]

78. Write a Java program to test that a given array of integers of length 2 contains a 4 or a 7. Go to the editor
Sample Output:

Original Array: [5, 7]
true

83. Write a Java program to multiply corresponding elements of two arrays of integers. Go to the editor
Sample Output:

Array1: [1, 3, -5, 4]

Array2: [1, 4, -5, -2]

Result: 1 12 25 -8
 */