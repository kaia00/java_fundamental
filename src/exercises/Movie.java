package exercises;

import java.util.Arrays;

public class Movie {
    private String name;
    private String type;
    private String[] actor;

    public Movie(String n, String t, String [] actor) {
        this.name = n;
        this.type = t;
        this.actor = actor;

    }


    public void show() {
        System.out.println(name);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //public String getActor(int index) {
     //   String actor;
      //  return actor;
   // }

    public void setActor(String[] actor) {
        this.actor = actor;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", actor=" + Arrays.toString(actor) +
                '}';
    }
}




