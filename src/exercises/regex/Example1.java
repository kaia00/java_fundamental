package exercises.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Example1 {
    public static void main(String[] args) {
        System.out.println(Pattern.matches(".s", "as"));
        System.out.println(Pattern.matches(".t", "dt"));
        System.out.println(Pattern.matches(".d", "odt"));
        System.out.println(Pattern.matches(".d", "oodt"));
        System.out.println(Pattern.matches("..t", "odt"));
        Pattern p = Pattern.compile("a*b");
        Matcher m = p.matcher("aaaaab");
        System.out.println(m.matches());
    }
}
