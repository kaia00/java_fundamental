package exercises.regex;

import java.util.regex.Pattern;

public class RegexExample {
    public static void main(String[] args) {

        // x?   x occurs once or not at all
        // x+


        System.out.println("? quantifier ....");
        System.out.println(Pattern.matches("[amn]?", "a"));
        System.out.println(Pattern.matches("[amn]?", "aaa"));
        System.out.println(Pattern.matches("[amn]?", "aammmnn"));
        System.out.println(Pattern.matches("[amn]?", "aazzta"));
        System.out.println(Pattern.matches("[amn]?", "am"));

        System.out.println(Pattern.matches("[amn]+", "a"));
        System.out.println(Pattern.matches("[amn]+", "aaa"));
        System.out.println(Pattern.matches("[amn]+", "aammmnn"));
        System.out.println(Pattern.matches("[amn]+", "aazzta"));
        System.out.println(Pattern.matches("[amn]+", "am"));

        System.out.println("* quantifier ....");


        System.out.println(Pattern.matches("[amn]*", "am"));






    }
}

/*	[abc]	->    a, b, or c (simple class)

        System.out.println(Pattern.matches("[amn]", "a"));//true (among a or m or n)
                System.out.println(Pattern.matches("[amn]", "ammmna"));//false (m and a comes more than once)

                //[^abc]	-> Any character except a, b, or c (negation)
                System.out.println(Pattern.matches("[^amn]", "v"));
                System.out.println(Pattern.matches("[^amn]", "a"));

//[a-zA-Z]	-> a through z or A through Z, inclusive (range)
//[a-d[m-p]] ->	a through d, or m through p: [a-dm-p] (union)
//[a-z&&[def]]	-> d, e, or f (intersection)
//[a-z&&[^bc]]	 -> a  through z, except for b and c: [ad-z] (subtraction)
//[a-z&&[^m-p]]   ->a through z, and not m through p: [a-lq-z](subtraction)
*/