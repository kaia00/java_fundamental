package exercises.regex;

import java.util.regex.Pattern;

public class RegexExample3 {
    public static void main(String[] args) {
        System.out.println(Pattern.matches("[123][0-9]{6}", "1232949"));
        System.out.println(Pattern.matches("[123][0-9]{6}", "9953038"));
    }
}
