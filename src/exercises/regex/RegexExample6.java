package exercises.regex;

/*Create a regular expression that accepts alphanumeric characters only.
Its length must be 5 characters long only.*/

















import java.util.regex.*;
class RegexExample6{
    public static void main(String[] args){
//        System.out.println(Pattern.matches("[a-zA-Z0-9]{5}", "qweqw"));//true
        System.out.println(Pattern.matches("[a-zA-Z0-9]{10}", "ASDFGHJj"));//false (more than 6 char)
//        System.out.println(Pattern.matches("[a-zA-Z0-9]{5}", "ASDs2"));//true
//        System.out.println(Pattern.matches("[a-zA-Z0-9]{5}", "ale%2"));//false (% is not matched)
    }}