package exercises.regex;

import java.util.regex.Pattern;

public class RegexExample2 {
    public static void main(String[] args) {
        System.out.println("metacharacters d...."); // d means digit. any digit, short of 0-9

        System.out.println(Pattern.matches("\\d", "abc"));
        System.out.println(Pattern.matches("\\d", "1"));
        System.out.println(Pattern.matches("\\d", "4443"));
        System.out.println(Pattern.matches("\\d", "323abc"));

        System.out.println(Pattern.matches("\\D*", "abc")); // true. non digit and comes zero or more times.



    }
}
