package exercises.regex;

public class RegexExample4 {


    public static void main(String[] args) {
        String input = "I'm a java dev" + "job available for software engineer";
        String fixedInput = input.replaceAll("(java|job|senior)", "<b>$1<b>");
        System.out.println(fixedInput);

        String regex = "(java|job|senior)";
        String replacement = "<b>$1<b>";

       System.out.println(stringReplace(input,regex,replacement));

    }

    public static String stringReplace(String input, String regex, String replacement) {
       String fixedInput = input.replaceAll(regex, replacement);

                        return fixedInput;

    }


}
