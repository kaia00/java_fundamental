package exercises.regex;

public class ReplaceUsingRegexExample {
    public static void main(String[] args) {
        String input = "hello I'm a java dev" +
                "java job available for senior software engineer";

        String regex = "(java|job|senior)";
        String replacement = "<b>$1</b>";
//        String fixedInput = input.replaceAll("(java|job|senior)", "<b>$1</b>");
//        System.out.println(fixedInput);
        System.out.println(stringReplace(input,regex,replacement));


    }

    public static String stringReplace(String in,String reg, String rep){
        String fixedInput =  in.replaceAll(reg,rep);
        return fixedInput;
    }
}
