package exercises.regex;

/*Create a regular expression that accepts 7 digit numeric characters
 starting with 1, 2 or 3 only.*/

import java.util.regex.*;
class RegexExample7{
    public static void main(String args[]){
//        System.out.println("by character classes and quantifiers ...");
        System.out.println(Pattern.matches("[123][0-9]{6}", "1232949"));
        System.out.println(Pattern.matches("[123][0-9]{6}", "9953038"));

        System.out.println(Pattern.matches("[123][0-9]{6}", "9953032"));
        System.out.println(Pattern.matches("[123][0-9]{6}", "6953032"));
        System.out.println(Pattern.matches("[123][0-9]{6}", "1853031"));

        System.out.println("by metacharacters ...");
        System.out.println(Pattern.matches("[123]{1}\\d{6}", "123123"));
        System.out.println(Pattern.matches("[123]{1}\\d{6}", "q23das"));

    }
}