package exercises;

import java.util.Scanner;

public class ExerciseClass1 {
 void doSomethingDefault(){}

    public static void main(String[] args) {
        ExerciseClass2 exercise = new ExerciseClass2();
        exercise.doSomethingElse();
    }

    public static class MathDemo {
        public static void main(String[] args) {

            System.out.println(Math.pow(2,3));
            System.out.println(Math.max(123,34));
            System.out.println(Math.abs(-123));
            System.out.println(Math.ceil(123.32));
            System.out.println(Math.round(123.32));
            Scanner scanner = new Scanner(System.in);

        }
    }
}
