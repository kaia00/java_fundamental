package exercises.loopsAndArrays;

import java.util.Scanner;
import java.util.Arrays;

public class ExerciseArrays2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[] arrayofInts = new int[5];

        for (int i = 0; i < arrayofInts.length; i++) {
            arrayofInts[i] = s.nextInt();

        }
        for (int i : arrayofInts) {
           System.out.println(i + " ");
       }


        Arrays.sort(arrayofInts);

        System.out.println(Arrays.toString(arrayofInts));

        int sum = 0;
        for (int i=0; i<arrayofInts.length; i++){
            System.out.println(arrayofInts[i]);
            sum = sum + arrayofInts[i];
            System.out.println(sum);

        }
        System.out.println(sum);

        }
    }
