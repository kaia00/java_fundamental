package exercises.loopsAndArrays;

import java.util.Scanner;

    public class ExerciseDoWhileLoops {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            int num;
            System.out.println("please enter a number");
            do {

              num = s.nextInt();
                if (num <= 0) {
                    System.out.println("please enter another number");
                }

            }
            while (num == 0 || num < 0);

            if (num > 51) {

                System.out.println("pass");

            }

            else {
                System.out.println("failed");
            }

            }
        }
