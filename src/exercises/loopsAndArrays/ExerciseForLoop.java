package exercises.loopsAndArrays;

public class ExerciseForLoop {
    public static void main(String[] args) {
        for (int num = 10; num > 0; num--) {
            System.out.println(num);
        }

        int sum = 0;
        for (int i = 1; i <= 10; i++) {
            sum = sum + i;
        }
        System.out.println(sum);

        for (int num = 1; num <= 100; num += 2) {
            System.out.println(num);
        }

        for (int num = 1; num <= 100; num++) {
            if (num % 2 != 0) {
                System.out.println(num);
            }

        }
        int result2 = 0;
        for (int num = 1; num <= 100; num += 2) {
            if (num % 5 == 0) {
                result2 = result2 + num;
                System.out.println(num);

            }
        }
        System.out.println(result2);
    }
}