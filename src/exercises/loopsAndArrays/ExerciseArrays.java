package exercises.loopsAndArrays;

public class ExerciseArrays {
    public static void main(String[] args) {

    String[] myArray = new String[]{"first", "exit", "third", "fourth", "fifth"};

    for (String i : myArray) {
        System.out.println(i + " ");
        if (i.equals("exit")){
            break;
        }
    }


}}

