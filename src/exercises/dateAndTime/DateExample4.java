package exercises.dateAndTime;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class DateExample4 {
    public static DayOfWeek whatDayofTheWeek (String date){
        DayOfWeek day = LocalDate.parse(date).getDayOfWeek();
        return day;

    }

    public static void main(String[] args)
    {
        System.out.println(whatDayofTheWeek("1989-03-25"));
    }
}
