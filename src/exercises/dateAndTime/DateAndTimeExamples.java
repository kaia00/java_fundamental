package exercises.dateAndTime;

import java.util.Calendar;
import java.util.Date;


public class DateAndTimeExamples {
    public static void main(String[] args) {


        // Date now = new Date();
        long millis = System.currentTimeMillis();
        Date now = new Date(millis);
        System.out.println(now);

        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime(); //convert Calendar to date
        System.out.println(date);

        cal.setTime(now); // convert Date to Calendar
        System.out.println(cal.get(Calendar.YEAR)); //2018
        System.out.println(cal.get(Calendar.DAY_OF_YEAR));
        System.out.println(cal.get(Calendar.DAY_OF_MONTH));
        System.out.println(cal.get(Calendar.HOUR_OF_DAY));


    }

}
