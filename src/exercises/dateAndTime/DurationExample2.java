package exercises.dateAndTime;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

public class DurationExample2 {
    public static long differenceBetweenDates (LocalTime time1, LocalTime time2 ){
       long differenceInSeconds = Duration.between(time1, time2).getSeconds();

        return differenceInSeconds;
    }

    public static void main(String[] args) {

   System.out.println(differenceBetweenDates(LocalTime.parse("05:50"), LocalTime.parse("05:51")));
    }
}
