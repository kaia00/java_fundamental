package exercises.dateAndTime;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class LocalTimeExample {
    public static void main(String[] args) {
        LocalTime now = LocalTime.now();

        LocalTime sixThirty = LocalTime.parse("06:30");
       LocalTime poolSeitse = LocalTime.of(6,30);

        System.out.println(poolSeitse);

        LocalTime sevenThirty = LocalTime.parse("06:30").plus(1, ChronoUnit.HOURS);

        int six = LocalTime.parse("06:30").getHour();

        int nelikendviis = LocalTime.parse("02:45").getMinute();
        System.out.println(nelikendviis);


        boolean isBefore = LocalTime.parse("06:30").isBefore(LocalTime.parse("07:30"));

        System.out.println(isBefore);
        System.out.println(now);
        System.out.println(sixThirty);
        System.out.println(sevenThirty);
        System.out.println(six);

    }
}
