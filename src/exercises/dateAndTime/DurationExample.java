package exercises.dateAndTime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DurationExample {
    public static void main(String[] args) {

        LocalTime initialTime = LocalTime.of(6,30,0);
        LocalTime finalTime = initialTime.plus(Duration.ofSeconds(30));

        System.out.println(Duration.between(initialTime,finalTime).getSeconds());

        System.out.println(ChronoUnit.SECONDS.between(initialTime,finalTime));

        LocalDateTime esimene = LocalDateTime.now();
        LocalDateTime teine = esimene.plus(Duration.ofMinutes(15));

        System.out.println(Duration.between(esimene, teine).getSeconds());


        System.out.println(ChronoUnit.MINUTES.between(esimene,teine));

    }
}
