package exercises.dateAndTime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class LocaldateExample {
    public static void main(String[] args) {

        LocalDate täna = LocalDate.now();
        System.out.println(täna);
        System.out.println(LocalDate.of(2000,12,2));
        System.out.println(LocalDate.parse("2018-05-06"));

        LocalDate ammu = LocalDate.now().minusYears(4);
        System.out.println(ammu);

        LocalDate eile = LocalDate.now().minusDays(1);
        System.out.println(eile);

        LocalDate eelmineKuuSamaPäev = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        System.out.println(eelmineKuuSamaPäev);

        DayOfWeek misPäevOli = LocalDate.parse("1950-01-16").getDayOfWeek();
        System.out.println(misPäevOli);

        boolean liigaasta = LocalDate.parse("2010-05-06").isLeapYear();
        System.out.println(liigaasta);

        boolean kasOnPärast = LocalDate.parse("2026-06-06").isAfter(LocalDate.parse("2027-07-07"));
        System.out.println(kasOnPärast);

      /*  LocalDate localDate = LocalDate.now();
        LocalDate.of(2017, 12, 31);
        LocalDate.parse("2017-12-31");
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate previousMonthSameDay = LocalDate.now().minus(1, ChronoUnit.MONTHS);
        DayOfWeek day = LocalDate.parse("2017-12-31").getDayOfWeek();
        boolean leapYear = LocalDate.now().isLeapYear();
       boolean isAfter = LocalDate.parse("2016-06-12").isAfter(LocalDate.parse("2014-06-21"));
*/
    }
}
