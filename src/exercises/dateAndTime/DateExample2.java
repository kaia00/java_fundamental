package exercises.dateAndTime;

import java.time.LocalDate;

public class DateExample2 {
    public static boolean compareDate (String date1, String date2){
        boolean isAfter = LocalDate.parse(date1).isAfter(LocalDate.parse(date2));
       return isAfter;

    }

    public static void main(String[] args) {

        System.out.println(compareDate("2015-05-12", "2010-06-21"));
    }

}
