package exercises.dateAndTime;

import java.time.LocalDate;

public class DateExample3 {
    public static boolean isLeapYear(){
        boolean isleapYear = LocalDate.now().isLeapYear();
        return isleapYear;
    }

    public static void main(String[] args) {

        System.out.println(isLeapYear());
    }
}
