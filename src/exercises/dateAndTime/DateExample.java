package exercises.dateAndTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateExample {


    public static LocalDateTime getTwoDaysAfter(LocalDateTime today) {
        return today.plusDays(2);

    }

    public static void main(String[] args) {
        LocalDateTime today = LocalDateTime.now();

        System.out.println(getTwoDaysAfter(today));

        LocalDate mingiPäev = LocalDate.now();

        System.out.println(getWeekAgo(mingiPäev));
    }

    public static LocalDate getWeekAgo (LocalDate nädalTagasi) {
        return nädalTagasi.minusDays(7);
    }
}
