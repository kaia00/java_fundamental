package exercises.dateAndTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public class LocalDateTimeExample {
    public static void main(String[] args) {

        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        LocalDateTime.of(2015, Month.FEBRUARY, 20,06,30);
        LocalDateTime.parse("2015-02-20T06:30:00");

        System.out.println(now.plusDays(1));
        System.out.println(now.minusHours(2));
        System.out.println(now.getMonth());
        System.out.println(now.getSecond());
    }
}
