package practice.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Unit1Exercise {

    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Kaia", "Palm", 30),
                new Person("Mari", "Mets", 45),
                new Person("Peeter", "Eeter", 56),
                new Person("Paul", "Pill", 44),
                new Person("Tiina", "Tulp", 13)
        );


     /*   Collections.sort(people, Comparator.comparing(Person::getLastname));

        performConditionally(people, p -> true, p -> System.out.println(p) );
        performConditionally(people, p -> p.getLastname().startsWith("P"), p -> System.out.println(p));

       */
        people.forEach(System.out::println);

        List<String> words = Arrays.asList("tere", "headaega", "misteed", "niisama");
        words.forEach(p -> System.out.print(p + " "));
        System.out.println("\n");
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(222);
        numbers.add(-545);

        numbers.forEach(System.out::println);

        people.stream()
                .filter(person -> person.getName().length()==4)
                .forEach(System.out::println);


    }

    private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
        for (Person p : people
        ) {
            if (predicate.test(p)) {
                consumer.accept(p);
            }


        }

    }


}

