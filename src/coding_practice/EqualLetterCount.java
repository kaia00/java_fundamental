package coding_practice;

import java.util.*;

public class EqualLetterCount {
    public static void main(String[] args) {

        EqualLetterCount equalLetterCount = new EqualLetterCount();
        System.out.println(equalLetterCount.isEqual("abav"));

    }

    public boolean isEqual(String s) {

        Map<Character, Integer> letters = new HashMap<>();

        for (Character c : s.toCharArray()) {

            if (letters.containsKey(c)) {
                letters.put(c, letters.get(c) + 1);
            } else {
                letters.put(c, 1);
            }

        }

        for (Character key : letters.keySet()) {
            if (letters.get(key) % 2 != 0) {
                return false;
            }

        }
        return true;
    }
}
