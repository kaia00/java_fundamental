package coding_practice;

import java.util.Stack;

class Te {

    public static void main(String[] args) {
        Parentheses p = new Parentheses();


        System.out.println(p.isValid("({)}"));
        System.out.println(p.isValid("({[)}"));
        System.out.println(p.isValid("(()()()()"));
        System.out.println(p.isValid("())()()()"));
        System.out.println(p.isValid(")()()()()"));
        System.out.println(p.isValid("(((()()()()"));
    }
}

public class Parentheses {

    public boolean isValid(String s) {

        Stack<Character> characterStack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {

            char c = s.charAt(i);

            if ((c == '(') || (c == '[') || (c == '{')) {
                characterStack.push(c);
            } else {
                if (characterStack.isEmpty()) {
                    return false;
                } else {
                    char c1 = characterStack.pop();

                    if (c == ')' && c1 != '(') {
                        return false;
                    }
                    if (c == ']' && c1 != '[') {
                        return false;
                    }
                    if (c == '}' && c1 != '{') {
                        return false;
                    }
                }
            }
        }

        return characterStack.isEmpty();
    }
}
