package coding_practice;


import java.util.HashMap;
import java.util.Map;

public class RecurringCharFinder {

    private char findRecurringChar(String word) {
        Map<Character, Integer> letters = new HashMap<>();

        for (char letter : word.toCharArray()) {
            if (letters.containsKey(letter)) {
                return letter;
            }
            letters.put(letter, 1);
        }
        return 0;
    }

    public static void main(String[] args) {
        RecurringCharFinder recurringCharFinder = new RecurringCharFinder();
        System.out.println(recurringCharFinder.findRecurringChar("ABBA"));
    }
}
