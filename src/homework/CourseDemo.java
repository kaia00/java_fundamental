package homework;

public class CourseDemo {
    public static void main(String[] args) {

        Course javaCourse = new Course();

        javaCourse.setCourseTitle("Introduction to Java for beginners");
        javaCourse.setCourseCode("JA4560");
        javaCourse.setAssessmentForm("Practical project + exam");
        javaCourse.setCredit(5.00);
        javaCourse.setLessonsPerWeek(2);
        javaCourse.setSemester("Spring 2019");

        System.out.println(javaCourse);
        System.out.print("\n");

        System.out.println("Title of the course: " + javaCourse.getCourseTitle());
        System.out.println("The code of the course: " + javaCourse.getCourseCode());
        System.out.println("The assessment form: " + javaCourse.getAssessmentForm());
        System.out.println("The number of credits for this course: " + javaCourse.getCredit());
        System.out.println("The number of lessons per week: " + javaCourse.getLessonsPerWeek());
        System.out.println("This course will take place: " + javaCourse.getSemester());

    }
}

