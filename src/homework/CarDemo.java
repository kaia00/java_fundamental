package homework;

import static homework.Car.numberOfCarsCreated;

public class CarDemo {
    public static void main(String[] args) {
        Car car1 = new Car("Volkswagen", "72 kW", true, 12.1);
        Car car2 = new Car("BMW", "200 kW", true, 5.6);
        Car car3 = new Car("Audi", "120 kW", true, 7.9);

        car1.setColor("Black");
        car1.setModel("Polo");
        car1.setProductionYear(2010);
        car1.setSpeed("170 km/h");

        car2.setColor("Red");
        car2.setModel("M3");
        car2.setProductionYear(2018);
        car2.setSpeed("250 km/h");

        car3.setColor("Grey");
        car3.setModel("A3");
        car3.setProductionYear(2005);
        car3.setSpeed("200 km/h");

        System.out.println("The number of cars created: " + numberOfCarsCreated());

        System.out.println(car1.getCarName());
        System.out.println("Color: " + car1.getColor());
        System.out.println("Model: " + car1.getModel());
        System.out.println("Production year: " + car1.getProductionYear());
        System.out.println("Speed: " + car1.getSpeed());

        System.out.println("Engine power: " + car1.getEnginePower());
        System.out.println("Has ABS? " + car1.isAbs());
        System.out.println("Acceleration: " + car1.getAcceleration());
        System.out.println("Max speed: " + car1.getMAX_SPEED());

        System.out.println("\n");

        System.out.println(car2.getCarName());
        System.out.println("Color: " + car2.getColor());
        System.out.println("Model: " + car2.getModel());
        System.out.println("Production year: " + car2.getProductionYear());
        System.out.println("Speed: " + car2.getSpeed());

        System.out.println("Engine power: " + car2.getEnginePower());
        System.out.println("Has ABS? " + car2.isAbs());
        System.out.println("Acceleration: " + car2.getAcceleration());

        System.out.println("\n");

        System.out.println(car3.getCarName());
        System.out.println("Color: " + car3.getColor());
        System.out.println("Model: " + car3.getModel());
        System.out.println("Production year: " + car3.getProductionYear());
        System.out.println("Speed: " + car3.getSpeed());

        System.out.println("Engine power: " + car3.getEnginePower());
        System.out.println("Has ABS? " + car3.isAbs());
        System.out.println("Acceleration: " + car3.getAcceleration());

    }
}
