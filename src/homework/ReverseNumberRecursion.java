package homework;

public class ReverseNumberRecursion {
    private void reverseNumberRecursive(int numberToReverse) {
        System.out.print(numberToReverse%10);
        if (numberToReverse > 9)
            reverseNumberRecursive(numberToReverse/10);
    }

    public static void main(String[] args) {
        ReverseNumberRecursion reverser = new ReverseNumberRecursion();
        int numberFromUserInput = 2019;
        System.out.println(numberFromUserInput);
        reverser.reverseNumberRecursive(numberFromUserInput);


    }
}
