package homework;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DateHW {
    public static void main(String[] args) {

        System.out.println(getDifferenceBetweenDays(LocalDate.parse("1989-03-25"), LocalDate.parse("2019-02-10")));

    }

    public static long getDifferenceBetweenDays(LocalDate date1, LocalDate date2) {

        long differenceInDays = ChronoUnit.DAYS.between(date1,date2);
        return differenceInDays;


    }
}

