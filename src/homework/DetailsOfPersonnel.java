package homework;

public class DetailsOfPersonnel {
    enum Level {
        SENIOR,
        JUNIOR,
        UNDEFINED
    }
    public static void main(String[] args) {
        String name = "Kaia Palm";
        Level nameLevel = Level.JUNIOR;
        String name2 = "Mari Mets";
        Level nameLevel2 = Level.SENIOR;
        String name3 = "Martin Tamm";
        Level nameLevel3 = Level.UNDEFINED;

        System.out.println(name);
        System.out.println("Position of " + name + " is " + nameLevel);
        System.out.println(name2);
        System.out.println("Position of " + name2 + " is " + nameLevel2);
        System.out.println(name3);
        System.out.println("Position of " + name3 + " is " + nameLevel3);


        char sex = 'f';
        int age = 29;
        int yearOfBirth = 1989;
        String monthOfBirth = "March";
        int dayOfBirth = 25;
        String address = "Tallinn";
        String position = "Lab assistant";
        String department = "Microbiology";
        String phone = "53440183";

        System.out.print("\n");
        System.out.println("Further details of " + name + " are: ");
        System.out.println(sex);
        System.out.println(age);
        System.out.println(yearOfBirth + ", " + monthOfBirth + " " + dayOfBirth);
        System.out.println(address);
        System.out.println(position);
        System.out.println(phone);
        System.out.println(department);


    }
}
