package homework;

import java.util.Scanner;

public class CalculatorDemo2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Calculator cal = new Calculator();

        System.out.println("Choose the operation from the list (type the corresponding number): 1: Add 2: Multiply 3: Subtract 4: Divide");

        int selection = s.nextInt();

        System.out.println("Please enter number 1");
        double num1 = s.nextDouble();
        System.out.println("Please enter number 2");
        double num2 = s.nextDouble();
        if (num2 == 0) {
            System.out.println("You cannot insert zero for second input, please enter another number");
            num2 = s.nextDouble();
        }
        double result;

        if (selection == 1) {
            result = cal.add(num1, num2);
            cal.printResult("The result of adding is: ", result);
        } else if (selection == 2) {
            result = cal.multiply(num1, num2);
            cal.printResult("The result of multyplying is: ", result);
        } else if (selection == 3) {
            result = cal.subtract(num1, num2);
            cal.printResult("The result of subtraction is: ", result);
        } else if (selection == 4) {
            result = cal.divide(num1, num2);
            cal.printResult("The result of division is: ", result);

        }
    }
}

