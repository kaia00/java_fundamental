package homework;

public class Car {

    static int carCounter;

    private String carName;
    private String enginePower;
    private boolean abs;
    private double acceleration;
    private final int MAX_SPEED = 250;

    public Car(String name, String enginePower, boolean abs, double acceleration) {
        carCounter++;
        this.carName = name;
        this.enginePower = enginePower;
        this.abs = abs;
        this.acceleration = acceleration;

    }

    private String speed;
    private int productionYear;
    private String color;
    private String model;

    public int getMAX_SPEED() {
        return MAX_SPEED;
    }

    public String getCarName() {
        return carName;
    }

    public String getEnginePower() {
        return enginePower;
    }

    public boolean isAbs() {
        return abs;
    }

    public double getAcceleration() {
        return acceleration;
    }


    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public static int numberOfCarsCreated() {
        return carCounter;
    }
}


