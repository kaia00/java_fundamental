package homework;

public class Calculator {


    public double add(double num1, double num2) {
        return num1 + num2;
    }

    public double multiply(double num1, double num2) {
        return num1 * num2;
    }

    public double subtract(double num1, double num2) {
        return num1 - num2;
    }

    public double divide(double num1, double num2) {
return num1/num2;
    }

    public void printResult(String message, double result) {
        System.out.println(message + " " + result);
    }
}


