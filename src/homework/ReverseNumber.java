package homework;

public class ReverseNumber {
    public static void main(String[] args) {
        int number = 2019;
        System.out.println(number);
        while (number >0) {
            System.out.print(number % 10);
            number = number/10;
        }
    }
}
