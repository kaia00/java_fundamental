package homework;

public class Course {


private String courseCode;
private String courseTitle;
private double credit;
private String semester;
private int lessonsPerWeek;
private String assessmentForm;


    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getLessonsPerWeek() {
        return lessonsPerWeek;
    }

    public void setLessonsPerWeek(int lessonsPerWeek) {
        this.lessonsPerWeek = lessonsPerWeek;
    }

    public String getAssessmentForm() {
        return assessmentForm;
    }

    public void setAssessmentForm(String assessmentForm) {
        this.assessmentForm = assessmentForm;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseCode='" + courseCode + '\'' +
                ", courseTitle='" + courseTitle + '\'' +
                ", credit=" + credit +
                ", semester='" + semester + '\'' +
                ", lessonsPerWeek=" + lessonsPerWeek +
                ", assessmentForm='" + assessmentForm + '\'' +
                '}';
    }
}





