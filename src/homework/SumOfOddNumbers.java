package homework;

public class SumOfOddNumbers {
    public static void main(String[] args) {

        int sum = 0;
        for (int num = 1; num <= 100; num += 2) {
            sum = sum + num;
        }
        System.out.println(sum);

    }
}

