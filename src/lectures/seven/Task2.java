package lectures.seven;

import java.util.Scanner;


public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter number 1:");
        int num1 = scanner.nextInt();
        System.out.println("Please enter number 2:");
        int num2 = scanner.nextInt();

        if (num1 == num2) {
            System.out.println("You entered " + num1 + " " + num2 + " .They are same.");
        } else if (num1 > num2) {
            System.out.println("You entered " + num1 + " " + num2 + " Bigger one is: " + num1);
        } else if (num2 > num1) {
            System.out.println("You entered " + num1 + " " + num2 + " Bigger one is: " + num2);
        }
    }
}
