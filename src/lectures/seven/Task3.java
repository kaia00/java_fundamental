package lectures.seven;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter name:");
        String name = scanner.nextLine();
        System.out.println(name.replaceAll(".(?=.)", "$0 "));

        for (int i = 0; i < name.length(); i++) {
            System.out.print(name.charAt(i) + " ");
        }


    }
}