package lectures.thirteen.oop.association.composition;

public class Mouse {

    private Button leftButton; // there is association between mouse and button object. Highly-coupled.
    // This association is a composition because there is part-of relation. Button is part of Mouse.
    private Button rightButton;
    private Gear gear;

    public Button getLeftButton() {
        return leftButton;
    }

    public void setLeftButton(Button leftButton) {
        this.leftButton = leftButton;
    }

    public Button getRightButton() {
        return rightButton;
    }

    public void setRightButton(Button rightButton) {
        this.rightButton = rightButton;
    }

    public Gear getGear() {
        return gear;
    }

    public void setGear(Gear gear) {
        this.gear = gear;
    }
}
