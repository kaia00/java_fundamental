package lectures.thirteen.oop.example;

public class Frog extends Animal {
    @Override
    public String eatingType() {
        return "Omnivore";
    }

    @Override
    public String breahingType() {
        return "Skin and lungs";
    }
}
