package lectures.thirteen.oop.example;

public class AnimalTest {

    public static void main(String[] args) {
        Animal dog = new Dog();
        Animal frog = new Frog();
        // Frog frog = new Frog(); võib ka nii kirjutada aga OOP põhimõttes on esimene variant parem.

        System.out.println("Dog's eating type: " + dog.eatingType());
        System.out.println("Frog's eating type: " + frog.eatingType());

    }


}
