package lectures.thirteen.oop.example;

public class Dog extends Animal{

    public boolean isBarking(){
        return true;
    }

    @Override
    public String eatingType() {
        return "Carnivore";
    }

    @Override
    public String breahingType() {
        return "lungs";
    }
}
