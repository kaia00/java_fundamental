package lectures.thirteen.oop.inheritance;


public class Car extends Vehicle { // only 1 extension is allowed.


    public String name = "car_name";

    public Car(int maxSpeed) {
        super(maxSpeed);  // super is pointing superclass - and superclass constructor.
    }

    @Override
    // can be used with methods, not fields. Override keyword is optional here but it is better readability. Annotations start with @.
    public String vehicleType() {
        return "small vehicle";
    }

    //   @Override can not be used here.
    public String vehicleType2() {
        return "";
    }

    public void printValues() {
        System.out.println(super.name);
        System.out.println(this.name);

        System.out.println(vehicleType());
    }

    public String getNameFromVehicle() {
        return super.name;
    }

    public static void main(String[] args) {
        Car car = new Car(65); // I am using parameterized constructor since super class has no default constructor.

        car.printValues();

    }
}
