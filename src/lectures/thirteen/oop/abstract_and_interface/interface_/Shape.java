package lectures.thirteen.oop.abstract_and_interface.interface_;

public interface Shape {

    // all shapes have these variables but they are all different from each other.

    double getArea(); // interface methods are public as defaul.
    double getPerimeter(); // interface methods can not be private or protected.

    default void printMyName(){
        System.out.println("My name is Kaia");  // an interface can have only 1 method type with body -> default type.

    }
}
