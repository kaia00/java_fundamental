package lectures.thirteen.oop.abstract_and_interface.interface_;

public class RectangleTest {
    public static void main(String[] args) {
        int edge1 = 5;
        int edge2 = 4;
        Rectangle rectangle = new Rectangle(edge1, edge2); // Also Shape recatangle is possible.

        System.out.println("Area: " + rectangle.getArea());
        System.out.println("Perimeter: " + rectangle.getPerimeter());

        System.out.println();
        System.out.println();

        rectangle.printMyName();
    }
}

// ntks List on ka interface, ei ole concrete. Luues peab täpsustama kas ArrayList, LinkedList jne.
