package lectures.thirteen.oop.abstract_and_interface.abstract_;

public abstract class Vehicle {

    private int maxSpeed;

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public abstract void move(); //abstract methods don't have a body.
}
