package lectures.thirteen.oop.abstract_and_interface.abstract_;

public class AbstractTest {
    public static void main(String[] args) {
        Vehicle car = new Car();
        car.move();

        Vehicle train = new Train();
        train.move();


        //POLYMMORPHISM POWER

        if (car instanceof Car){  // changing an object to another object.
            car = new Train();
            car.move();
        }
    }
}
