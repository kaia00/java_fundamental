package lectures.thirteen.oop.polymorphism;

public class SubClass1 extends SuperClass1 {

    @Override
    public String getBar(){
        return "Bar_value_from_SubClass";
    }
}
