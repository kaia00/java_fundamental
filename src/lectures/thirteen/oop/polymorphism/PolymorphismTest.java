package lectures.thirteen.oop.polymorphism;

public class PolymorphismTest {
    public static void main(String[] args) {
        SuperClass1 superClass1 = new SuperClass1(); // superClass1 is instance of only SuperClass.
        System.out.println(superClass1.getBar());

        SuperClass1 superClass2 = new SubClass1(); //this is polymorphism! subClass2 on is instance of Subclass1 and Superclass.
        System.out.println(superClass2.getBar());

        SubClass1 subClass1 = new SubClass1();  // subClass1 is instance of Subclass1
        System.out.println(subClass1.getBar());

        if (superClass2 instanceof SuperClass1) {   // usage of polymorphism power.
            System.out.println("I am a super class");
        }
        if (superClass2 instanceof SubClass1) {
            System.out.println("I am a subclass");
        }
    }
}
