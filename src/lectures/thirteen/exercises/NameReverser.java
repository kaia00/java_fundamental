package lectures.thirteen.exercises;

public class NameReverser {

    public String getReverseEasily(String name){
        return new StringBuilder(name).reverse().toString();
    }

    public String getReverse(String name){
        String reversedName = "";
        for (int i = name.length()-1; i >= 0; i--) {
            reversedName = reversedName + name.charAt(i);

        }
        return  reversedName;
    }
    public static void main(String[] args) {
        NameReverser nameReverser = new NameReverser();

        System.out.println(nameReverser.getReverseEasily("KAia"));
        System.out.println(nameReverser.getReverse("Kaia"));


     /*
        String name = "Kaia";

        String reversed = "";

        for (int i = name.length() - 1; i >= 0; i--) {
            reversed = reversed + name.charAt(i);
        }
        System.out.println(reversed);
        */

    }

}
