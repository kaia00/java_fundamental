package lectures.thirteen.exercises;

public class SumOfPrimeNumbers {

    public boolean isPrime(int num) {
        int counter = 0;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                counter++;
            }
        }
        if (counter == 2) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        SumOfPrimeNumbers sumOfPrimeNumbers = new SumOfPrimeNumbers();

        int result = 0;
        for (int i=1; i<=10; i++){
            if (sumOfPrimeNumbers.isPrime(i)){
                result += i; // result = result + i;
            }
        }
        System.out.println(result);

       /* long sum = 0;
        for (int i = 2; i <= 1000; i++) {
            if (isPrime(i)) {
                sum += i;
            }
        }
        System.out.println(sum);
    }


    private static boolean isPrime(final int number) {
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
        */
    }
}


/*
Task 2 : find sum up of prime numbers until 1000
 */

