package lectures.thirteen.homework;

public class FruitDemo {
    public static void main(String[] args) {
        Fruit lemon = new Lemon();
        Fruit orange = new Orange();
        Fruit apple = new Apple();

        System.out.println("The color of lemon is usually " + lemon.getColor() + ", the taste is usually " + lemon.getTaste());
        System.out.println("The color of orange is usually " + orange.getColor() + ", the taste is usually " + orange.getTaste());
        System.out.println("The color of apple can be " + apple.getColor() + ", the taste can be " + apple.getTaste());


    }
}
