package lectures.thirteen.homework;

public class Orange extends Fruit {

    @Override
    public String getColor() {
        return "orange";
    }

    @Override
    public String getTaste() {
        return "sweet";
    }


}
