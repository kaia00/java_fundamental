package lectures.thirteen.homework;

public class Apple extends Fruit {

    @Override
    public String getColor() {
        return "green or yellow or red";
    }

    @Override
    public String getTaste() {
        return "sweet or sour";
    }


}
