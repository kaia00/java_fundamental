package lectures.thirteen.homework;

public class Lemon extends Fruit {
    @Override
    public String getColor() {
        return "yellow";
    }

    @Override
    public String getTaste() {
        return "sour";
    }


}
