package lectures.thirteen.enums;

public enum LengthUnit { // enums are set of constant variables.

    METER(1),
    DECIMETER(0.1),
    CENTIMETER(0.01),
    MILLIMETER(0.001);

    // ; to finish enum scope. to use it with values, you need to change constructor. then calling is with -getValue.

    LengthUnit(double value) {
        this.value = value;
    }

    private double value;

    public double getValue() {
        return value;
    }


}
