package lectures.thirteen.enums;

public enum WeatherType {

    RAINY ("vihmane"),
    SUNNY ("päikeseline"),
    SNOWY ("lumine"),
    WINDY("tuuline");

    private String weatherType;

    WeatherType(String weatherType) {
        this.weatherType = weatherType;

    }



    @Override
    public String toString() {
        return "Ilm on: " + weatherType;
    }

    // standardizing values
}
