package lectures.fourteen.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionsTest {

    public void handleCeckedExceptionByTryCatch() { // handling checked exceptions is mandattory. try-catch block
        // is required.
        try {
            File file = new File("wrong path"); // path should be like this: /users/kaia/projects/tallinn5
            FileReader fileReader = new FileReader(file);
        } catch (FileNotFoundException e) {  // good to put specific exception here. "e" here is a variable name.
            e.printStackTrace();
        }
    }

    public void handleCheckedExceptionByThrowingExceptionFromMethodSignature() throws FileNotFoundException {
        File file = new File("wrong path");
        FileReader fileReader = new FileReader(file);
    }

    public void handleUncheckedExceptionByTryCatch() { //handling unchecked exceptions is not mandatory. try-catch block is
        // optional.

        try {
            int a = 5 / 0;   // java doesn't know the result until you run the program.
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        ExceptionsTest exceptionsTest = new ExceptionsTest();

        exceptionsTest.handleCeckedExceptionByTryCatch();
        exceptionsTest.handleCheckedExceptionByThrowingExceptionFromMethodSignature();
        exceptionsTest.handleUncheckedExceptionByTryCatch();
    }
}
