package lectures.fourteen.exceptions;

public class Car {

    private int speed;

    public Car(int speed) {
        this.speed = speed;
    }

    public void increaseSpeed() throws CarCrashedException {

        try {
            speed = speed + 70;
            if (speed>200){
                throw new CarCrashedException(this); //this kewyword is used to point out class itself
            }

        } catch (CarCrashedException e){
            e.printStackTrace();
        }
        speed = speed + 70;
        if (speed > 200) {
            throw new CarCrashedException(this);

        }
    }
}

// saab teha kahte moodi! try catch või throws.