package lectures.fourteen.exceptions;

public class ThrowingException {

    public void throwExceptionExample() {
        try {

            int a = 5 / 0;

        } catch (ArithmeticException e) {
            // throwing exception is mostly used for giving information to developer for maintenance of the code.
            throw new ArithmeticException("You are probably dividing by zero (0).");

        }
    }

    public static void main(String[] args) {
        ThrowingException throwingException = new ThrowingException();

        throwingException.throwExceptionExample();
    }
}
