package lectures.fourteen.exceptions;

import lectures.fourteen.exceptions.Car;
import lectures.fourteen.exceptions.CarCrashedException;

public class CarTest {
    public static void main(String[] args) throws CarCrashedException {
        Car car = new Car(50);
        car.increaseSpeed();
        car.increaseSpeed();
        car.increaseSpeed();
    }
}
