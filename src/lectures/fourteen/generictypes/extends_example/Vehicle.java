package lectures.fourteen.generictypes.extends_example;

public abstract class Vehicle {

    public abstract void repair();
}
