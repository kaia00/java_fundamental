package lectures.fourteen.generictypes.extends_example;

public class Airplane extends Vehicle {

    @Override
    public void repair() {
        System.out.println("Airplane has been repaired.");
    }
}
