package lectures.fourteen.generictypes.extends_example;

public class Car extends Vehicle {
    @Override
    public void repair() {
        System.out.println("Car has been repaired");
    }
}
