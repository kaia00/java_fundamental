package lectures.fourteen.generictypes.basics;

import lectures.fourteen.generictypes.basics.Airplane;
import lectures.fourteen.generictypes.basics.Car;
import lectures.fourteen.generictypes.basics.GenericBox;

public class GenericBoxTest {
    public static void main(String[] args) {
        Car car = new Car();

        GenericBox<Car> aBoxWithCarInIt = new GenericBox<>(car);
        aBoxWithCarInIt.getItem().getMaxSpeed();  // reach without casting.

        Airplane airplane = new Airplane();
        GenericBox<Airplane> aBoxWithAirplaneInIt = new GenericBox<>(airplane);
        aBoxWithAirplaneInIt.getItem().getWingSpan();


    }
}
