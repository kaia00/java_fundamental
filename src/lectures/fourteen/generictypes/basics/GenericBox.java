package lectures.fourteen.generictypes.basics;

public class GenericBox<T> {

    T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public GenericBox(T item) {
        this.item = item;
    }
}
