package lectures.fourteen.generictypes.basics;

import lectures.fourteen.generictypes.basics.Airplane;
import lectures.fourteen.generictypes.basics.Car;
import lectures.fourteen.generictypes.basics.NormalBox;

public class NormalBoxTest {
    public static void main(String[] args) {

        Car car = new Car();
        NormalBox normalBox = new NormalBox(car);

        Car carFromBox = (Car) normalBox.getItem(); // i could reach the car object by casting

        Airplane airplaneFromBox = (Airplane) normalBox.getItem();  // example of unsafe casting.

    }
}

