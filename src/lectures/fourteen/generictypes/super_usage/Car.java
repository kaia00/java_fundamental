package lectures.fourteen.generictypes.super_usage;

public class Car extends Vehicle {

    private int maxSpeed;

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Car(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
