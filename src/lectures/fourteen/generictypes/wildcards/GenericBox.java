package lectures.fourteen.generictypes.wildcards;

public class GenericBox<T> {

    T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public GenericBox(T item) {
        this.item = item;
    }
}
