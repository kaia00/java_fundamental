package lectures.fourteen.exercises;

public class FindFibonacci {


    public int getNthFibonacci(int nth) {


        int fibo1 = 1;
        int fibo2 = 1;
        int fibo3 = 2;
        if (nth == 1 || nth == 2) {
            return fibo1;
        }
        if (nth == 3) {
            return fibo3;
        }

        int counter = 4;
        while (true) {
            fibo1 = fibo2 + fibo3;
            if (counter == nth) {
                return fibo1;
            }
            counter++;

            fibo2 = fibo1 + fibo3;
            if (counter == nth) {
                return fibo2;
            }
            counter++;
            fibo3 = fibo1 + fibo2;
            if (counter == nth) {
                return fibo3;
            }
            counter++;


        }
    }

    public static void main(String[] args) {
        FindFibonacci findFibonacci = new FindFibonacci();

        int nth = 7;
        System.out.println(findFibonacci.getNthFibonacci(nth));
    }
}

// find nth fibonacci