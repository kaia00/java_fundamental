package lectures.nine;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CreateLinkedListExample {
    public static void main(String[] args) {
        LinkedList<String> friends = new LinkedList<>();

        friends.add("Liina");
        friends.add("Siim");
        friends.add("Piret");
        friends.add("Terje");

        System.out.println(friends);

        friends.add(3, "Mati");
        System.out.println(friends);

        friends.addFirst("Mari");
        System.out.println(friends);

        friends.addLast("Kati");
        System.out.println(friends);

        List<String> dissapearedPersonList = new ArrayList<>();
        dissapearedPersonList.add("Elvis");

        friends.addAll(dissapearedPersonList);
        System.out.println(friends);
    }

}
