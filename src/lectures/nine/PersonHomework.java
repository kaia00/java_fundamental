package lectures.nine;

import java.util.ArrayList;

// main method. create 5 person objects. put them in the arraylist. then print them out.

public class PersonHomework {
    public static void main(String[] args) {
        Person person1 = new Person("Kaia", 29);
        Person person2 = new Person("Ats", 31);
        Person person3 = new Person("Lenna", 2);
        Person person4 = new Person("Lennart", 5);
        Person person5 = new Person("Mari", 16);

        ArrayList<Person> listofPeople = new ArrayList<>();  // Arraylist that contains "Person" objects.

        listofPeople.add(person1);
        listofPeople.add(person2);
        listofPeople.add(person3);
        listofPeople.add(person4);
        listofPeople.add(person5);

        // how to print out one person (2 ways):

        Person firstPerson = listofPeople.get(0);
        System.out.println(firstPerson.getName() + " " + firstPerson.getAge());

        System.out.println(listofPeople.get(0).getName() + " " + listofPeople.get(0).getAge());


        // 2 ways to print out the elements of the list (for each loop and usual for loop):


        for (Person someone:listofPeople){ // arraylist is based on Person class.
            System.out.println(someone.getName() + " " + someone.getAge());
        }
        for (int counter = 0; counter<listofPeople.size(); counter++){
            System.out.println(listofPeople.get(counter).getName() + " " + listofPeople.get(counter).getAge());
        }

        System.out.println(person2);
    }
}

