package lectures.nine;

import java.util.ArrayList;
import java.util.List;

public class CreateArrayListExample {
    public static void main(String[] args) {
        List<String> listofAnimals = new ArrayList<>();

        listofAnimals.add("dog");
        listofAnimals.add("cat");
        listofAnimals.add("tiger");
        listofAnimals.add("bear");
        listofAnimals.add("monkey");


        System.out.println(listofAnimals);

        if(listofAnimals.contains("donkey")){
            System.out.println("Why donkey is here??");
        }
        else{
            System.out.println("donkey doesn't exist");
        }

        listofAnimals.add(4, "eagle");
        System.out.println(listofAnimals);

        List<String> newListofAnimals = new ArrayList<>();
        newListofAnimals.addAll(listofAnimals);
        System.out.println(newListofAnimals);



    }

}
