package lectures.nine;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue<String> waitingQueue = new LinkedList<>();

      waitingQueue.add("Kaia");
      waitingQueue.add("Ats");
      waitingQueue.add("Lennart");
      waitingQueue.add("Lenna");

        System.out.println(waitingQueue);

        String name = waitingQueue.remove();
        System.out.println(name);

        name = waitingQueue.poll(); // poll - returns null rather than exception (if empty)! (vs remove)
        System.out.println(name);
        System.out.println(waitingQueue);
    }
}
