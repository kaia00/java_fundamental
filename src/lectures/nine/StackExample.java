package lectures.nine;

import java.util.Stack;

public class StackExample {
    public static void main(String[] args) {
        Stack<String> stackOfCards = new Stack<>();

        stackOfCards.push("Jack");
        stackOfCards.push("Queen");
        stackOfCards.push("Ace");
        stackOfCards.push("King");

        System.out.println(stackOfCards);


    }
}
