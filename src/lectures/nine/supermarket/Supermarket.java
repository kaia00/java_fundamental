package lectures.nine.supermarket;

import java.util.ArrayList;
import java.util.List;

public class Supermarket {

    private List<Product> listOfProducts = new ArrayList<>();

    public List<Product> getListofProducts() {
        return listOfProducts;
    }

    public void setListofProducts(List<Product> listofProducts) {
        this.listOfProducts = listofProducts;
    }

    public void printAllProducts() {

        for (Product product : listOfProducts) {
            System.out.println("Product name: " + product.getName() + " Price: " + product.getPrice());
        }

    }

    public int getIndexOfProduct(String incomingProductName) {
        for (int i = 0; i < listOfProducts.size(); i++) {
            Product product = listOfProducts.get(i);
            String nameOfThisProduct = product.getName();
            if (nameOfThisProduct.equalsIgnoreCase(incomingProductName)) {
                return i;
            }
        }
        return -1;
    }

    public void insertProduct(Product incomingProduct) {
        listOfProducts.add(incomingProduct);
    }

    public void removeProduct(int index) {
        listOfProducts.remove(index);
    }
}
