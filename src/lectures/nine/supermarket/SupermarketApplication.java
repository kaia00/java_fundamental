package lectures.nine.supermarket;

import java.util.Scanner;

public class SupermarketApplication {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String item;
        String selection;
        Supermarket supermarket = new Supermarket();
        Account account = new Account();
        double budget;

        System.out.println("Welcome, admin!");
        while (true) {
            System.out.println("What do you want to do - add, remove or be done with it?");
            selection = scanner.nextLine();

            if (selection.equalsIgnoreCase("done")) {
                break;
            } else if (selection.equalsIgnoreCase("add")) {
                System.out.println("What do you want to add?");
                item = scanner.nextLine();
                if (supermarket.getIndexOfProduct(item) == -1) {
                    System.out.println("What is the price?");
                    String price = scanner.nextLine();
                    double price1 = Double.parseDouble(price);
                    Product product1 = new Product(item, price1);
                    supermarket.insertProduct(product1);

                    System.out.println("You added: " + item + ", price: " + price);
                } else {
                    System.out.println("This item already exists.");

                }

            } else if (selection.equalsIgnoreCase("remove")) {
                System.out.println("What do you want to remove");
                item = scanner.nextLine();
                if (supermarket.getIndexOfProduct(item) == -1) {
                    System.out.println("Product does not exist.");
                } else {
                    supermarket.removeProduct(supermarket.getIndexOfProduct(item));
                    System.out.println("Product removed");
                }
            }
        }

        supermarket.printAllProducts();

        System.out.println("Please enter your budget.");
        String budget1 = scanner.nextLine();
        budget = Double.parseDouble(budget1);
        account.setBalance(budget);

        while (true) {
            System.out.println("What do you want to buy? If done, write done");
            item = scanner.nextLine();
            if (item.equalsIgnoreCase("done")) {
                break;
            }
            int indexOfProduct = supermarket.getIndexOfProduct(item);
            if (indexOfProduct == -1) {
                System.out.println("Product does not exist");
            } else {
                SuperMarketUtilities superMarketUtilities = new SuperMarketUtilities();
                Product selectedProduct = supermarket.getListofProducts().get(indexOfProduct); // mida see rida tähendab?
                System.out.println("How many of " + selectedProduct.getName() + " do you want?"); // ja see ka??

                String amount1 = scanner.nextLine();
                int amount = Integer.parseInt(amount1);

                double totalCost = selectedProduct.getPrice() * amount;
                totalCost = superMarketUtilities.round(totalCost);
                boolean isEnoughMoney = account.deduct(totalCost);
                if (isEnoughMoney) {
                    System.out.println("You bought: " + selectedProduct.getName());
                    account.addUpdateProduct(selectedProduct.getName(), amount);
                }

            }
        }


        account.printAccountInfo();

    }
}


/*  main method. it will start the app. 1. write welcome to the supermarket admin. 2. what to you want to do
 * add, remove, done. done - move on. add - add a new product to the supermarket memory. you need a name then check
 * if name already exists in the supermarket memory. if exists, say "it already exists". ask again, what do you want to do.
 * if the user inputted name does not exist in memory (listofproducts), then ask for the price. then just put it in the supermarket
 * memory. after added, write added.
 * remove means it will remove the supermarket memory (list). is does not exist, say it does not exist. if exists, remove it and say it.
 *
 * print out all the items and prices.
 *
 * create an account. Pls enter your budget. assign to this account. ask what do you want to buy. if done write done. if the
 * product name user inserted does not exist in the supermarket then say it doesnt exist. ask again.
 * if exists, ask how many user wants to buy. check if there is enough money if there is, then add the product to the account with the amount
 * if not, ask again what do you want to buy.
 *
 * after you are done print what you have bought and remaining balance.
 * */