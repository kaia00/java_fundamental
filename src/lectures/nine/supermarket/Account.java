package lectures.nine.supermarket;

import java.util.ArrayList;

public class Account {

    private double balance;

   // public Account(double balance) {
    //    this.balance = balance;
  //  }

    ArrayList<Product> listOfProducts = new ArrayList<>(); // list of bought products.

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public ArrayList<Product> getListOfProducts() {
        return listOfProducts;
    }

    public void setListOfProducts(ArrayList<Product> listOfProducts) {
        this.listOfProducts = listOfProducts;
    }

    public void printAccountInfo() {

        for (Product boughtProduct : listOfProducts) {
            System.out.println("Product name: " + boughtProduct.getName() + " Quantity: " + boughtProduct.getQuantity());

          //  for (int i = 0; i < listOfProducts.size() ; i++) {
         //       listOfProducts.get(i).getName()
          //  }
        }
        System.out.println("Remaining balance is: " + balance);
    }

    public boolean deduct(double cost) {
        double threshold = .0001;
        SuperMarketUtilities superMarketUtilities = new SuperMarketUtilities();
        if (Math.abs(balance - cost) < threshold) {
            balance = balance - cost;
            balance = superMarketUtilities.round(balance);
            return true;
        } else if (balance > cost) {
            balance = balance - cost;
            balance = superMarketUtilities.round(balance);
            return true;
        } else {
            System.out.println("Not enough money on your account. Choose something else. ");
            return false;
        }
        // if (cost <= balance) {
        //     balance = balance - cost;
        //   return true;
        //   }else {
        //     System.out.println("Not enough money on your account");
        //     return false;


    }

    public void addUpdateProduct(String productName, int amount) {

        for (Product boughtExistingProduct: listOfProducts) {
            if(boughtExistingProduct.getName().equalsIgnoreCase(productName)){
                int updatedAmount = boughtExistingProduct.getQuantity() + amount;
                boughtExistingProduct.setQuantity(updatedAmount);
                return;
            }
        }
        Product newProduct = new Product();
        newProduct.setName(productName);
        newProduct.setQuantity(amount);
        listOfProducts.add(newProduct);

        // or with "break" and boolen isExisting...nii saab ka teha.


      //  for (int i = 0; i < listOfProducts.size(); i++) {
        //    if (listOfProducts.get(i).getName().equalsIgnoreCase(productName)) {


        //        return;
           // }
        }


    }



//balance, arraylist of Product. getters and setters.Method of printAccountInfo -> print information of bought product names and quantitites and lastly
// the balance of the account.


//deduct method which will have cost as parameter and it will return true or false. purpose is to check if you have enough money.
// if you have, it will deduct - true.
//addUpdateProduct -> add to product array list. if it exists already it will increase the quantity. This method will not return anything - void.