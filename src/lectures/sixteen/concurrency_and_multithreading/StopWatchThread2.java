package lectures.sixteen.concurrency_and_multithreading;

public class StopWatchThread2 implements Runnable { // same as extends Thread. If you need to extend another class, implement Runnable instead.

    private String prefix;

    public StopWatchThread2(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(prefix + ": " + i);
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
