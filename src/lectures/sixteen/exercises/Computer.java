package lectures.sixteen.exercises;

import java.io.*;

public class Computer implements Serializable {

    private String brand;  // apple
    private String opSystem; // macOs
    private String type; //laptop

    public Computer(String brand, String opSystem, String type) {
        this.brand = brand;
        this.opSystem = opSystem;
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOpSystem() {
        return opSystem;
    }

    public void setOpSystem(String opSystem) {
        this.opSystem = opSystem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
