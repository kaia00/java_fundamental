package lectures.sixteen.exercises;

public class HelloWorld {
    public static void main(String[] args) throws InterruptedException {
        HelloWorldThread helloWorldThread = new HelloWorldThread();
        System.out.println("Hello");
        helloWorldThread.start();
        Thread.sleep(3000);
        System.out.println("World");
    }
}
