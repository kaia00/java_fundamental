package lectures.sixteen.exercises;

import java.io.*;

public class ComputerSerialization {


    public void readFile(String readingPath) {

        Computer computer;

        try {

            File file = new File(readingPath);
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            computer = (Computer) objectInputStream.readObject();

            System.out.println("Deserialized brand: " + computer.getBrand());
            System.out.println("Deserialized op system: " + computer.getOpSystem());
            System.out.println("Deserialized type: " + computer.getType());

            objectInputStream.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void writeFile(String writingPath, Computer computer) {

        try {
            File file = new File(writingPath);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);


            objectOutputStream.writeObject(computer);
            objectOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ComputerSerialization computerSerialization = new ComputerSerialization();
        Computer computer = new Computer("apple", "MacOs", "laptop");
        String path = "/Users/kaiapalm/java_fundamental1/computer.txt";
        computerSerialization.writeFile(path, computer);

        computerSerialization.readFile(path);
    }
}

