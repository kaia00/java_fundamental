package lectures.sixteen.exercises;

/*
- take multiple inputs from user keyboard and print it to a file.
(when user type a sentence and press enter, print that sentence to file)
- remind user to write "nl" for a new line in the file
(if user input nl and then press enter, then put a new line to your file)
- remind user to write "EoF" to finish file writing and close the program.
(if user input EoF and then press enter, then close file and finish

 */

// ei tööta :(


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PrintUserInputToFile {

    public static void main(String[] args) {
        PrintUserInputToFile printUserInputToFile = new PrintUserInputToFile();
        String writingPath = "/Users/kaiapalm/java_fundamental1/text_files/userInput.txt";
        Scanner scanner = new Scanner(System.in);
        String userInput;

        StringBuilder stringBuilder = new StringBuilder();
        while (true) {
            System.out.println("Enter a sentence.");
            userInput = scanner.nextLine();
            if (userInput.equals("EoF")) {
                break;
            } else if (userInput.equals("nl")) {
                stringBuilder.append("\n");

            } else {
                stringBuilder.append(userInput);
            }

        }

        printUserInputToFile.writeFile(writingPath, stringBuilder.toString());

    }

    public void writeFile(String pathString, String content) {
        try {

            Path path = Paths.get(pathString);

           List<String> linesToWrite = Arrays.asList(content);

            Files.write(path, linesToWrite, StandardOpenOption.APPEND);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
