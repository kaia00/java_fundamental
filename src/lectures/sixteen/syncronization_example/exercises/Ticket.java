package lectures.sixteen.syncronization_example.exercises;

public class Ticket {

    private int ticketCount;

    public Ticket(int ticketCount) {
        this.ticketCount = ticketCount;
    }

    public synchronized void sellTicket(){
        if (ticketCount > 0) {
            System.out.println("You bought a ticket");
            ticketCount--;
            System.out.println("Tickets available: " + ticketCount);
        } else {
            System.out.println("Tickets are sold out. ");
        }
    }


}
