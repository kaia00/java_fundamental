package lectures.sixteen.syncronization_example.exercises;

public class TicketUsage {

    public static void main(String[] args) {
        Ticket ticket = new Ticket(1);
        TicketSellerThread ticketSellerThread1 = new TicketSellerThread(ticket);
        TicketSellerThread ticketSellerThread2 = new TicketSellerThread(ticket);
        TicketSellerThread ticketSellerThread3 = new TicketSellerThread(ticket);
        TicketSellerThread ticketSellerThread4 = new TicketSellerThread(ticket);

        ticketSellerThread1.start();
        ticketSellerThread2.start();
        ticketSellerThread3.start();
        ticketSellerThread4.start();
    }
}
