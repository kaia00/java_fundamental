package lectures.sixteen.syncronization_example.exercises;

public class TicketSellerThread extends Thread {

    private Ticket ticket;

    public TicketSellerThread(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public void run() {
        ticket.sellTicket();
    }
}
