package lectures.sixteen.homework;

import java.io.*;

public class LinesReverser {

    /*
    public List<String> getFileLines (String pathString){
    List<String> lines = new ArrayList<>();
    try{
    Path path = Paths.get(pathString);
    lines = Files.readAllLines(path);
    } catch (IOException e){
    e.printStackTrace();
    }
    } return lines;

    main:
    String pathString = "";
    List<String> lines = lineReverser.getFileLines(pathString);


     */

    public String readLines(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            File file = new File(path);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();

    }

    public void writeLines(String path, String lines) {
        try {
            File file = new File(path);
            FileWriter fileWriter = new FileWriter(file);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            for (int i = 0; i < lines.length(); i++) {
                printWriter.print(reverseLines(lines).charAt(i));

            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // fix it to return reversed LINES not only one line..


    private String reverseLines(String line) {

        String reversedLine = "";
        for (int i = line.length() - 1; i >= 0; i--) {
            reversedLine = reversedLine + line.charAt(i);

        }

        return reversedLine;
    }

    public static void main(String[] args) {
        LinesReverser linesReverser = new LinesReverser();
        String readingPath = "/Users/kaiapalm/Desktop/inputFile.txt";
        String writingPath = "/Users/kaiapalm/Desktop/outputFile.txt";
        String lines = linesReverser.readLines(readingPath);
        linesReverser.writeLines(writingPath, lines);
    }
}




/*
1 - Read a file and reverse all lines and print to another file by starting from last line to first line.
For example;
-----inputFile.txt-----
I am a developer.
I will develop my own application.
I will gain lots of software skills.
-----outputFile.txt-----
.slliks erawtfos fo stol niag lliw I
.noitacilppa nwo ym poleved lliw I
.repoleved a ma I
 */