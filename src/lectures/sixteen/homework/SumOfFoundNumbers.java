package lectures.sixteen.homework;

// PS! Ei tööta, kui number on Stringis kõige viimasel kohal. Kas Aare oskab seda parandada?

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SumOfFoundNumbers {

    public String readLines(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            File file = new File(path);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                line = bufferedReader.readLine();

            }

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();

    }

    public static void main(String[] args) {

        SumOfFoundNumbers sumOfFoundNumbers = new SumOfFoundNumbers();
        String readingPath = "/Users/kaiapalm/Desktop/inputFile2.txt";
        String lines = sumOfFoundNumbers.readLines(readingPath);

        String numberFromString = "";
        int sum = 0;
        int counter = 0;
        for (int i = 0; i < lines.length(); i++) {

            char c = lines.charAt(i);
            if (Character.isDigit(lines.charAt(i))) {
                numberFromString = numberFromString + lines.charAt(i);
                if (!numberFromString.equals("")) {
                    sum = sum + Integer.parseInt(numberFromString);
                    counter++;
                    numberFromString = "";
                }
            }

        }
        System.out.println("There are " + counter + " numbers. Sum of them is " + sum);

    }
}

/*
2 - Read a file an find all numbers then print out that how many numbers are there and what is the sum up. Then print them to console
For example;
-----inputFile.txt-----
There are 45 different options on menu in Restaurant No.14
I like almos half of them. Especially 5 hours baked mushrooms.
-----console output-----
There are 3 numbers
Sum up of them is : 64
 */
