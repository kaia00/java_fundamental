package lectures.sixteen.new_input_output;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class NewIOWritingExample {

    public void writeFile(String pathString) {
        try {

            Path path = Paths.get(pathString);

            //   List<String> linesToWrite = new ArrayList<>();
            //   linesToWrite.add("Kaia");
            //  linesToWrite.add("Palm");
            List<String> linesToWrite = Arrays.asList("Kaia", "Palm"); // doing same thing as above with 3 lines.

            Files.write(path, linesToWrite, StandardOpenOption.APPEND); // if you don't put APPEND as a parameter, file will be overwritten.
         // CREATE creates a new file.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        NewIOWritingExample newIOWritingExample = new NewIOWritingExample();
        String path = "/Users/kaiapalm/java_fundamental1/testtest.txt";
        newIOWritingExample.writeFile(path);

    }
}
