package lectures.sixteen.new_input_output;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NewIOUSages {

    public static void main(String[] args) throws IOException {
        String pathString = "/Users/kaiapalm/java_fundamental1/test";
        Path path = Paths.get(pathString);


        System.out.println(Files.isDirectory(path)); // returns true if directory exists. if you want to create a directory
        // first check if it already exists.

        if (!Files.isDirectory(path)) {
            Files.createDirectory(path); // createDirectory method creates a directory if it doesn't exist.
            // if the directory already exists, JVM throws FileAlreadyExistsException.

        }
        // ctrl + J shows documentation

        String existingFilePathString = "/Users/kaiapalm/java_fundamental1/testtest.txt";
        String symLinkPathString = "";
        Path symLinkPath = Paths.get(symLinkPathString);
        Path existingFilePath = Paths.get(existingFilePathString);
        // creating shortcut... ?



    }


}
