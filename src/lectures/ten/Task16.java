package lectures.ten;

import java.util.ArrayList;
import java.util.Scanner;

public class Task16 {
    public static void main(String[] args) {
        String name;
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> students = new ArrayList<>();
        System.out.println("Welcome to school!");
        while (true) {
            System.out.println("What to you want to do? Add/Remove/Search/View/Done-to exit");
            String selection = scanner.nextLine();
            if (selection.equalsIgnoreCase("Done")) {
                break;
            } else if (selection.equalsIgnoreCase("add")) {
                System.out.println("Enter student name");
                name = scanner.nextLine();
                students.add(name);
                System.out.println(name + " is added.");
            } else if (!selection.equalsIgnoreCase("add") && !selection.equalsIgnoreCase("remove") && !selection.equalsIgnoreCase("search") && !selection.equalsIgnoreCase("view")) {
                System.out.println("Invalid command");
            } else if (selection.equalsIgnoreCase("remove")) {
                System.out.println("Who you wanna remove?");
                name = scanner.nextLine();
                if (students.contains(name)) {
                    students.remove(name);
                    System.out.println(name + " is removed.");
                } else {
                    System.out.println(name + " does not exist. ");
                }


            } else if (selection.equalsIgnoreCase("view")){
                System.out.println(students);
            } else if (selection.equalsIgnoreCase("search")){
                int counter=0;
                System.out.println("Enter student name");
                name = scanner.nextLine();
                for (String studentName: students) {
                    if (studentName.equalsIgnoreCase(name)){
                        counter++;
                    }

                }
                System.out.println("Total occurrence: " + counter);
            }
        }
    }
}

/* Arraylist operations...

create class with main

create Arraylist which will contain the name of the students.

You will have commands. Add/Remove/Done/search/view

You can add name of student (even there are 2 Andreis you will add it. There is no duplicate checking.
you can remove student if exists. Otherwise print no student.Case sensetive.
you can search for student. If exists tell how many students of same name exists. Case insesitive.
view will print all the info. */