package lectures.recap;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task16 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to school");
        List<String> studentNames = new ArrayList<>();
        boolean forever = true;
        while (forever) {
            System.out.println("What do you want to do? Add/Remove/Search/View/Done");


            String command = scanner.nextLine();

            if (command.equalsIgnoreCase("Done")) {
                break;
            } else if (command.equalsIgnoreCase("add")) {
                System.out.println("Enter student name");
                String name = scanner.nextLine();

                studentNames.add(name);
                System.out.println(name + " is added.");


            } else if (command.equalsIgnoreCase("remove")) {
                System.out.println("Enter student name");
                String name = scanner.nextLine();
                if (studentNames.contains(name)) {
                    studentNames.remove(name);
                    System.out.println(name + " is removed.");
                } else {
                    System.out.println("There is no such student in our database.");
                }
            } else if (command.equalsIgnoreCase("search")) {
                System.out.println("Enter student name.");
                String name = scanner.nextLine();
                int counter = 0;
                for (int i = 0; i < studentNames.size(); i++) {

                    if (studentNames.get(i).equalsIgnoreCase(name)) {
                        counter++;

                    }
                }
                System.out.println("Total ocurrence " + counter);


            } else if (command.equalsIgnoreCase("view")) {
                System.out.println(studentNames);
            } else {
                System.out.println("invalid command");
            }

        }
    }
}

/*
Arraylist operations...

create class with main

create Arraylist which will contain the name of the students.

You will have commands. Add/Remove/Done/search/view

You can add name of student (even there are 2 Andreis you will add it. There is no duplicate checking.
you can remove student if exists. Otherwise print no student.Case sensetive.
you can search for student. If exists tell how many students of same name exists. Case insesitive.
view will print all the info.
 */