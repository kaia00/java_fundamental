package lectures.recap;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Task18 {

    public static void main(String[] args) {

        HashMap<String, Double> itemsHashMap = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to storage.");
        while (true) {
            System.out.println("What do you want to do? Add/Remove/Search/View/Done");
            String command = scanner.nextLine();
            if (command.equalsIgnoreCase("done")) {
                break;
            } else if (command.equalsIgnoreCase("Add")) {
                System.out.println("Enter storage item");
                String itemName = scanner.nextLine();
                System.out.println("Enter item price");
                double itemPrice = Double.parseDouble(scanner.nextLine());
                if (itemsHashMap.containsKey(itemName)) {
                    System.out.println(itemName + " already exists.");
                    continue;
                }
                itemsHashMap.put(itemName, itemPrice);
                System.out.println(itemName + " is added. Its price is: " + itemPrice);
            } else if (command.equalsIgnoreCase("view")) {
                if (itemsHashMap.isEmpty()) {
                    System.out.println("Nothing to show here.");
                    continue;
                }
                System.out.println("Printing out items and prices.");
                for (Map.Entry<String, Double> entry : itemsHashMap.entrySet()) {
                    System.out.println(entry.getKey() + " => " + entry.getValue());
                }
            } else if (command.equalsIgnoreCase("search")) {
                if (itemsHashMap.isEmpty()) {
                    System.out.println("Nothing to search here..iz empty");
                    continue;
                }
                System.out.println("Enter storage item");
                String itemToSearch = scanner.nextLine();
                boolean found = false;
                for (Map.Entry<String, Double> entry : itemsHashMap.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(itemToSearch)) {
                        if (!found) {
                            found = true;
                        }
                        System.out.println("Found " + entry.getKey() + " => " + entry.getValue());
                    }
                }
                if (!found) {
                    System.out.println("Item not found.");

                }
            } else if (command.equalsIgnoreCase("remove")) {
                if (itemsHashMap.isEmpty()) {
                    System.out.println("Is empty, nothing to remove here.");
                    continue;
                }
                System.out.println("Write item to remove");
                String itemToRemove = scanner.nextLine();
                if (itemsHashMap.containsKey(itemToRemove)) {
                    itemsHashMap.remove(itemToRemove);
                    System.out.println(itemToRemove + " is removed.");
                } else {
                    System.out.println("Item does not exist. ");
                }

            }
        }
    }
}






/*
HashMap operations...

create class with main

create HashMap which will contain the name of the items.

You will have commands. Add/Remove/Done/search/view

you can add item
name of item and price  No duplicate names for case sensitive.  You can add apple and Apple but not Apple 2 times.

you can remove item
name of the item. case sensitive.

search will find according to name. It is case insetive.

view will view all.

 */