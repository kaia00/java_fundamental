package lectures.recap;

import java.util.HashMap;
import java.util.Map;

public class Dog {

    private String name;
    private int age;
    private String species;

    @Override
    public String toString() {
        return
                name + " ," +
                 age + " ," +
                 species
                ;
    }

    public Dog() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }
}

/*

HashMap operations...

create dog class.
dog class will have
name age and species field.

You will have commands. Add/Remove/Done/search/view

Add dog case sensitive. Meaning you can add 2 dogs same name (Lucky or lucky will be accepted) Exact same name is not accepted.

Remove dog case sensitive. remove dog.

Search dog case insesitive. print all dog information.

view print data.

 */