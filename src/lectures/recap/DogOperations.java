package lectures.recap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DogOperations {

    Map<String, Dog> dogHashMap = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        DogOperations dogOp = new DogOperations();
        System.out.println("Welcome to dog administration.");

        while (true) {
            System.out.println("What do you want to do? Add/Remove/View/Search/Done");
            Dog dog = new Dog();
            String action = scanner.nextLine();
            if (action.equalsIgnoreCase("done")) {
                break;
            } else if ((!action.equalsIgnoreCase("add") && !action.equalsIgnoreCase("remove") && !action.equalsIgnoreCase("view")) && !action.equalsIgnoreCase("search")) {
                System.out.println("invalid command");
            } else if (action.equalsIgnoreCase("add")) {
                System.out.println("Enter dog name");
                String name = scanner.nextLine();
                if (dogOp.dogHashMap.containsKey(name)) {
                    System.out.println("Dog already exists.");
                    continue;
                }
                dog.setName(name);
                System.out.println("Enter dog age");
                int age=0;
               try {
                   age = Integer.parseInt(scanner.nextLine());
               } catch (NumberFormatException e){
                   System.out.println("Sa ei sisestanud numbrit!");
                   e.printStackTrace();

               }
                dog.setAge(age);
                System.out.println("Enter dog species");
                String species = scanner.nextLine();
                dog.setSpecies(species);
                dogOp.add(name, dog);
            } else if (action.equalsIgnoreCase("remove")) {
                if (dogOp.dogHashMap.isEmpty()) {
                    System.out.println("No dogs, nothing to remove.");
                    continue;
                }
                System.out.println("Enter dog name");
                String name = scanner.nextLine();


                if (dogOp.dogHashMap.containsKey(name)) {
                    dogOp.remove(name, dog);
                } else {
                    System.out.println(name + " does not exist.");
                }
            } else if (action.equalsIgnoreCase("view")) {
                if (dogOp.dogHashMap.isEmpty()) {
                    System.out.println("No dogs to show");
                } else {
                    dogOp.printAllDogs();
                }

            } else if (action.equalsIgnoreCase("search")) {
                if (dogOp.dogHashMap.isEmpty()) {
                    System.out.println("no dogs to search");
                    continue;
                }
                System.out.println("Enter dog name");
                String name = scanner.nextLine();

                boolean found = false;
                for (Map.Entry<String, Dog> entry : dogOp.dogHashMap.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(name)) {
                        if (!found) {
                            found = true;
                            System.out.println("System found dog.");
                        }
                        dog = entry.getValue();
                        System.out.println(entry.getKey() + " => " + dog.getName() + " ," + dog.getAge() + " ," + dog.getSpecies());
                    }

                }
                if (!found) {
                    System.out.println("no such dog.");
                }

            }
        }

    }


    void add(String dogToAdd, Dog dog) {

        dogHashMap.put(dogToAdd, dog);
        System.out.println("Dog is added");
        System.out.println("Dog name: " + dog.getName() + " Age:" + dog.getAge() + " Species: " + dog.getSpecies());
    }

    void remove(String dogToRemove, Dog dog) {
        dogHashMap.remove(dogToRemove);
        System.out.println(dogToRemove + " is removed");
    }

    void printAllDogs() {
        System.out.println("Printing all dogs:");
        for (Map.Entry<String, Dog> entry : dogHashMap.entrySet()) {
            System.out.println(entry.getKey() + " => " + entry.getValue());

        }
    }
}


/*

You will have commands. Add/Remove/Done/search/view

Add dog case sensitive. Meaning you can add 2 dogs same name (Lucky or lucky will be accepted) Exact same name is not accepted.

Remove dog case sensitive. remove dog.

Search dog case insesitive. print all dog information.

view print data.

 */