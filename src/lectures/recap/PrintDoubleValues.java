package lectures.recap;

public class PrintDoubleValues {
    public static void main(String[] args) {

        double cost = 1.09;
        int howManyEuros = (int) cost;
        double howManyCents = cost % 1;
        System.out.print("Toode maksab " + howManyEuros + " eurot ja ");
        System.out.printf("%.0f", howManyCents * 100);
        System.out.println(" senti.");


    }
}
