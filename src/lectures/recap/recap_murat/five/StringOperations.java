package lectures.recap.recap_murat.five;

public class StringOperations {

    public static void main(String[] args) {
        String helloWorld = "Hello, World";

        System.out.println(helloWorld.substring(1, 5));
        System.out.println(helloWorld.substring(5));

        int lengthOfString = helloWorld.length();
        System.out.println(lengthOfString); //12

        int indexOfComma = helloWorld.indexOf(",");
        System.out.println(indexOfComma); //5

        int lastInexofO = helloWorld.lastIndexOf("o");
        System.out.println(lastInexofO); //8

        int lastIndexOfBigO = helloWorld.lastIndexOf("O");
        System.out.println(lastIndexOfBigO); // error


    }
}
