package lectures.recap.recap_murat.four;

public class CastOperations {

    public static void main(String[] args) {
        float someDoubleNumber = 223.454f;
        int someIntNumber;

        someIntNumber = (int) someDoubleNumber;
        System.out.println(someIntNumber);

        char someChar = 'C';
        someIntNumber = (int) someChar;
        System.out.println(someIntNumber);

        float someFloatNumber = ((10*(12+5))*10)/2;
        System.out.println(someFloatNumber);
    }


}
