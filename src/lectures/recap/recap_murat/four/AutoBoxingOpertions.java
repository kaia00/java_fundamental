package lectures.recap.recap_murat.four;

public class AutoBoxingOpertions {
    public static void main(String[] args) {


        int someNumber = 1;
        Integer someObjectInteger = someNumber;
        Integer someObjectInteger1 = new Integer(someNumber);

        int anotherInteger = someObjectInteger;

        System.out.println(someNumber + " " + someObjectInteger1 + " " + anotherInteger + " " + someObjectInteger);

        float someDoubleNumber = 2324.99f;
        Float objectFloatNumber = someDoubleNumber;
        System.out.println(objectFloatNumber);
        System.out.println(objectFloatNumber.intValue());

        int anotherNumber = 23;
        Integer anotherNumber1 = 23;
        if (anotherNumber == anotherNumber1) {
            System.out.println("These numbers are same");
        } else {
            System.out.println("These numbers are not same");
        }

        Integer number1 = 1;
        Integer number2 = 2;
        if (number1 == number2) {
            System.out.println("These are same");
        }
        Double double1 = 123.29;
        Double double2 = 145.3;

        int result = double2.compareTo(double1);
        System.out.println(result);

        // 0: if d1 is numerically equal to d2.
        //Negative value: if d1 is numerically less than d2.
        //Positive value: if d1 is numerically greater than d2.

        String kaiaName = "Kaia";
        kaiaName = "Murat";
        int a = 1;
        a = 2;
        a = 3;

        String name = "Kaia";
        String anotherName  = "Kaia";
        if (name == anotherName){
            System.out.println("Kaia prints out");
        }

        String myName = "Murat";
        String withNewOne = new String("Murat");
        if (myName == myName) {
            System.out.println("true1");
        }
        if (myName == withNewOne) {
            System.out.println("true2");
        }

        String anotherString = "Murat";

        if (myName == anotherString) {
            System.out.println("It actually prints");
        }
        if (myName == "Murat") {
            System.out.println("This will never print");
        }
        if (myName.equals("Murat")) {
            System.out.println("Hello " + myName);
        }
        if (myName.equalsIgnoreCase("murat")) {
            System.out.println("Hello again " + myName);
        }

    }
}
