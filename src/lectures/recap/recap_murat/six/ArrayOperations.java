package lectures.recap.recap_murat.six;

import java.util.Arrays;

public class ArrayOperations {

    public static void main(String[] args) {

        int[] arrayOfInts = {1, 2, 3, 4, 5, 6};

        for (int item : arrayOfInts) {
            System.out.print(item + " ");
        }
        System.out.println();
        System.out.println(Arrays.toString(arrayOfInts));

        // print all except if the value is 1:

        for (int item : arrayOfInts) {
            if (item != 1) {
                System.out.print(item);
            }
        }
        // print all except if the value is 1 use continue;

        for (int item : arrayOfInts) {
            if (item == 1) {
                continue;
            } else {
                System.out.println(item);
            }
        }

        // print items until 4 shows up use break

        for (int item : arrayOfInts) {
            if (item == 4) {
                break;
            } else {
                System.out.println(item);
            }
        }

    }
}



