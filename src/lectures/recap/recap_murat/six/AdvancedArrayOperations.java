package lectures.recap.recap_murat.six;

import java.util.Arrays;
import java.util.Scanner;

public class AdvancedArrayOperations {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] vegetables = {"Carrot", "Potato", "Onion", "Garlic", "Eggplant"};
        String[] fruits = {"Banana", "Orange", "Grape", "Apple", "Kiwi", "Pineapple"};

        String[] customerList = new String[7];
        System.out.println("Please enter vegetable or fruit");
        System.out.println("Vegetables =>" + Arrays.toString(vegetables));
        System.out.println("Fruits=>" + Arrays.toString(fruits));
        System.out.println();

        for (int counter = 0; counter < customerList.length; counter++) {
            customerList[counter] = scanner.next(); // old coder way?

            //new coder way.
       /* customerList[0] = scanner.next();
        customerList[1] = scanner.next();
        customerList[2] = scanner.next();
        customerList[3] = scanner.next();
        customerList[4] = scanner.next();
        customerList[5] = scanner.next();
        customerList[6] = scanner.next();*/


        }

        // find out the amount of vegetables

        int vegetableAmount = 0;
        int fruitAmount = 0;

        /*
         * First for is responsible for the customer Item loop
         * Meaning first we get the first item of the customer
         * then we check if it is a member of fruit or vegetable
         * array.
         */

        for (String customerItem : customerList) {
            System.out.println("Deciding " + customerItem + " if fruit or vegetable");
            //this one will decide if it is a vegetable:
            for (int counter = 0; counter < vegetables.length; counter++) {
                if (customerItem.equalsIgnoreCase(vegetables[counter])) {
                    vegetableAmount++;
                    System.out.println(customerItem + " is a vegetable");
                    break;

                }

            }
            for (int counter = 0; counter < fruits.length; counter++) {
                if (customerItem.equalsIgnoreCase(fruits[counter])) {
                    fruitAmount++;
                    System.out.println(customerItem + " is a fruit");
                    break;
                }

            }
        }

        if ((vegetableAmount + fruitAmount) != 7) {
            System.out.println("there is something un the list that is not a fruit or vegetable");
        } else {
            System.out.println("Vegetables: " + vegetableAmount + ", fruits: " + fruitAmount);
        }


    }
}

