package lectures.eleven.harrypotter;

import java.util.regex.Pattern;

public class CharacterCreation {

        public boolean validateName(String firstName) {
            if (firstName == null || firstName.isEmpty()) {
                System.out.println("First name is not suitable. You did not enter any name");
                return false;
            }
            if (firstName.contains(" ")) {
                System.out.println("First name must not contain any space");
                return false;
            }
            String filteredName = eraseNonLetters(firstName);
            if (!firstName.equalsIgnoreCase(filteredName)) {
                System.out.println("First name is not suitable. No numbers/Punctuations");
                return false;
            }
            return true;
        }
        public boolean validateSurname(String surname){
            if (surname == null || surname.isEmpty()) {
                System.out.println("Surname is not suitable. You did not enter any name");
                return false;
            }
            String filteredName = eraseNonLetters(surname);
            if (!surname.equalsIgnoreCase(filteredName)) {
                System.out.println("Surname is not suitable. No numbers/Punctuations");
                return false;
            }
            return true;
        }
        private String eraseNonLetters(String word){
            //Original value Murat1!
            String result = word.replaceAll("\\d",""); //Murat!
            result = result.replaceAll("\\p{Punct}",""); //Murat
            return result;
        }
    }

    /*


  static  boolean validateName(String firstName) {

        if (firstName == null ||firstName.isEmpty()){
            System.out.println("First name is not suitable. You did not enter any name");
            return false;
        }

        if(firstName.contains(" ")){
            System.out.println("First name must not contain any space");
            return false;
        }
        String filteredName = validateName((firstName)){

      }


    }

    boolean validateSurname(String surName) {
        if () {
            return true;
        } else {
            return false;
        }

    }

    private String eraseNonLetters(String word) {
        String result = word.replaceAll("")
                result.replaceAll()

    }

*/







/*
method validateName parameter will be firstName... It does not accept any space and it will not accept any
numbers or punctuation.
"First name is not suitable. You did not enter any name"
"First name must not contain any space"
"First name is not suitable. No numbers/Punctuations"
it will return true or false. If true this first name is acceptable. Otherwise return false.

method validateSurname parameter will be firstName... It does not accept any space and it will not accept any
numbers or punctuation.
"Surname is not suitable. You did not enter any name"
"Surname is not suitable. No numbers/Punctuations"
it will return true or false. If true this first name is acceptable. Otherwise return false.
method eraseNonLetters it gets parameter as string. Then it will filter the number and punctuation.

 */