package lectures.eleven.harrypotter;

import java.util.HashMap;
import java.util.Map;

public class Spells {

    Map<String, Spell> spells = new HashMap<>();


    public Map<String, Spell> getSpells() {
        return spells;
    }

    public void setSpells(Map<String, Spell> spells) {
        this.spells = spells;
    }

    public Spells() {

        spells.put("Lacarnum Inflamarae", new Spell("Lacarnum Inflamarae", 25, 10, 16, 70, 1));
        spells.put("Lumos Solem", new Spell("Lumos Solem", 45, 17, 23, 65, 2));
        spells.put("Everte Statum", new Spell("Everte Statum", 45, 23, 29, 60, 3));
        spells.put("Arania Exumai", new Spell("Arania Exumai", 50, 30, 36, 55, 4));
        spells.put("Avada Kedavra", new Spell("Avada Kedavra", 100, 100, 100, 10, 5));
        spells.put("Vulnera Sanentur", new Spell("Vulnera Sanentur", 25, 10, 20, 70, 1));

    }

    public Spell getSpell(String spellName) {
        Spell spell = spells.get(spellName);
        if (spell == null) {
            System.out.println("No such spell");
            return null;
        } else {
            return spell;
        }
    }


}

/*
Spells Class
Contains the spells
Lacarnum Inflamarae => 25(Price),10(min),16(max),70(success),1(speed rate)
Lumos Solem => 45,17,23,65,2
Everte Statum => 45,23,29,60,3
Arania Exumai => 50,30,36,55,4
Avada Kedavra => 100,100,100,10,5
Vulnera Sanentur => 25,10,20,70,1
getSpell Method and parameter will be SpellName(String) and it will return the Spell Object.


 */