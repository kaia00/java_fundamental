package lectures.eleven.harrypotter;


import java.util.Scanner;

public class WizardWars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Wizard Wars.");
        CharacterCreation character = new CharacterCreation();
        Wizard wizard = new Wizard();
        WizardShop wizardShop = new WizardShop();
        while (true) {
            System.out.println("Please enter first name");
            String firstName = scanner.nextLine();
            if (character.validateName(firstName)) {
                wizard.setName(firstName);
                break;
            }
        }
        while (true) {
            System.out.println("Please enter last name");
            String lastName = scanner.nextLine();
            if (character.validateSurname(lastName)) {
                wizard.setSurname(lastName);
                break;
            }
        }
        System.out.println("Welcome " + wizard.getName() + " " + wizard.getSurname());

        while (true) {
            System.out.println("Welcome to the Wizard Shop. What do you want to buy. Write done, if you are ready.");
            wizardShop.printAllSpells();
            String spellToBuy = scanner.nextLine();
            if (spellToBuy.equalsIgnoreCase("done")) {
                break;
            }
            wizardShop.buy(spellToBuy, wizard);

        }
        System.out.println(wizard.getSpellNames());



    }

}
