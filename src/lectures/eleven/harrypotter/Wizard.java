package lectures.eleven.harrypotter;

import java.util.ArrayList;
import java.util.List;

public class Wizard {

    private int health = 100;
    private int money = 100;
    private String name;
    private String surname;

    List<String> spellNames = new ArrayList<>();

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<String> getSpellNames() {
        return spellNames;
    }

    public void setSpellNames(List<String> spellNames) {
        this.spellNames = spellNames;
    }
}

/*
Create Wizard
health 100 default value
money 100 default value
name and surname
it will have knowledge of the spell names he bought.
 */