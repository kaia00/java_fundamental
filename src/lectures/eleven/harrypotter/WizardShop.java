package lectures.eleven.harrypotter;

import java.util.HashMap;
import java.util.Map;

public class WizardShop {

    Map<String, Integer> spellShop = new HashMap<>();

    public WizardShop() {

        spellShop.put("Lacarnum Inflamarae", 25);
        spellShop.put("Lumos Solem", 45);
        spellShop.put("Everte Statum", 45);
        spellShop.put("Arania Exumai", 50);
        spellShop.put("Avada Kedavra", 100);
        spellShop.put("Vulnera Sanentur", 25);
    }

    public void printAllSpells() {

        System.out.println(spellShop);

    }

    public void buy(String requestedSpell, Wizard wizard) {

        if (wizard.getSpellNames().contains(requestedSpell)) {
            System.out.println("You know this spell already");
            return;
        } else if (!spellShop.containsKey(requestedSpell)) {
            System.out.println("No such spell in the shop!");
            return;
        } else if (wizard.getMoney() < spellShop.get(requestedSpell)) {
            System.out.println("Not enough money.");
            return;
        } else {
            System.out.println("You bought " + requestedSpell);
            int costOfSpell = spellShop.get(requestedSpell);
            int remainingMoney = wizard.getMoney() - costOfSpell;
            wizard.setMoney(remainingMoney);
            wizard.getSpellNames().add(requestedSpell);

        }


        // kas on olemas juba
        // kas on poes olemas
        // kas on raha piisavalt / or needs more money
        // update..cost of the spell and balance. and add spell to knowledge -> arraylist.


    }


}

/*
printAllSpells method it will not have any parameter. it will just print
buy method will be void... Its parameters are String requestedSpell and Wizard object.
 */