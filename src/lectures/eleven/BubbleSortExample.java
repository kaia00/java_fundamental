package lectures.eleven;

import java.util.Arrays;

public class BubbleSortExample {
    public static void main(String[] args) {
        int numberArrays[] = {6, 5, 3, 1, 8, 7, 2, 4};
        System.out.println("Array before Bubble sort");
        System.out.println(Arrays.toString(numberArrays));

        bubbleSorter(numberArrays);
        System.out.println("Array after bubble sort");
        System.out.println(Arrays.toString(numberArrays));

    }

    static void bubbleSorter(int[] arrayToSort) {
        int totalSize = arrayToSort.length;
        int temp=0;
        for (int i = 0; i < arrayToSort.length-1; i++) {
            for (int j = 1; j < (totalSize-i) ; j++) {
                if(arrayToSort[j-1] > arrayToSort[j]){
                    //swap elements
                    temp = arrayToSort[j-1];
                    arrayToSort[j-1] =arrayToSort[j];
                    arrayToSort[j] = temp;
                }

            }

        }

       // for (int i = 0; i < arrayToSort.length-1 ; i++) {
        //    if (arrayToSort[i] > arrayToSort [i+1]) {
         //       int temp = i+1;
         //       arrayToSort[i] = temp;
            }

        }





