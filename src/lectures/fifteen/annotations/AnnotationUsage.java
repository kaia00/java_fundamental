package lectures.fifteen.annotations;

/**
 * Following objects can be annotated
 * Class
 * Field
 * Method
 * Method Parameter
 */

@Deprecated // this annotation is used to point out that this class has newer version.
public class AnnotationUsage {

    // there are some code quality tools like SonarCube, PMD, CheckStyle, Spotless.
    // is you use SuppressWarnings then code quality tools will not check this part.
    @SuppressWarnings("Unchecked")
    public void aMethod() {

    }

    public static void main(String[] args) {
        AnnotationUsage annotationUsage = new AnnotationUsage(); // the class name is crossed out because the class is depricated.
    }
}
