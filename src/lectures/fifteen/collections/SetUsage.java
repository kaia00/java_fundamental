package lectures.fifteen.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetUsage {

    public static void main(String[] args) {

        Set<String> names = new HashSet<>();

        names.add("Kaia");
        names.add("Ats");
        names.add("Silver");

        System.out.println(names);

        names.add("Kaia");

        System.out.println(names); //order is shuffled. No duplicate values allowed in a set!

        for (String name : names) {  // there is no get method because there is no index. only loops for iterating (and iterator) and
            //getting values.
            // why use set? because there are no duplicate values. If try to add duplicate values, it checks hash value and sees
            // it already exists.
            System.out.println(name);
        }

        Iterator<String> namesIterator = names.iterator();
        while (namesIterator.hasNext()) {
            System.out.println(namesIterator.next());
        }
    }
}
