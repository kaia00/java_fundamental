package lectures.fifteen.collections;

import java.util.*;

public class MapUsage {
    public static void main(String[] args) {
        Map<Integer, String> idCodeAndNameMap = new HashMap<>();

        idCodeAndNameMap.put(123, "Kaia");
        idCodeAndNameMap.put(126, "Ats");
        idCodeAndNameMap.put(125, "Silver ");
        idCodeAndNameMap.put(123, "Kaia Palm"); // "Kaia Palm" overrides "Kaia" because Keys are same. Keys can't be duplicated.
        System.out.println(idCodeAndNameMap);
        System.out.println("123s value is: " + idCodeAndNameMap.get(123));

        for (Integer key : idCodeAndNameMap.keySet()) {
            System.out.println(idCodeAndNameMap.get(key));
        }

        List<String> namesList = new ArrayList<>();
        namesList.addAll(idCodeAndNameMap.values()); // values of a map can be transferred to a list
        System.out.println(namesList);

        Set<String> namesSet = new HashSet<>();
        namesSet.addAll(idCodeAndNameMap.values()); // values of a map can be transferred to a set
        System.out.println(namesSet);


    }
}
