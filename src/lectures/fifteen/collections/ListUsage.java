package lectures.fifteen.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListUsage {

    public static void main(String[] args) {
        List<String> names = new ArrayList<>();  // polymorphism. this way is better.1)  from reading perspective - clearer.
        // 2) can convert it to LinkedList for example (is extending List).
        // ArrayList<String> names2 = new ArrayList<>(); another way but not so good.

        names.add("Kaia");
        names.add("Alex");
        names.add("Cristian");
        names.add("Alex");

        System.out.println("List elements: " + names);
        System.out.println("1st index of list: " + names.get(0));

        // print out list elements by for loop:

        for (String elementOfList : names) {
            System.out.println(elementOfList);
        }

        System.out.println("Size of list: " + names.size());
        System.out.println("Does the list contain Alex? " + names.contains("Alex"));
        System.out.println("Does the list contain Fargo?" + names.contains("Fargo"));
        names.remove("Kaia");
        System.out.println(names);
        System.out.println("Does the list contain Kaia? " + names.contains("Kaia"));
        //names.remove("Alex");
        names.remove(2); // it will work as above
        System.out.println(names);

        //ctrl + space shows all methods.

        // Print out List elements by Iterator:

        Iterator<String> iteratorOfNames = names.iterator();
        while (iteratorOfNames.hasNext()) {
            System.out.println(iteratorOfNames.next());
        }


    }
}
