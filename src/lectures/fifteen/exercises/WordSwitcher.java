package lectures.fifteen.exercises;

/*
- Read a file and group words 2 by 2 in each line and switch them. then print to a new file.
 - For example, Hello my name is Michael Jackson. => my Hello is name Jackson. Michael
 - If words count is odd in a line then put last word to head of sentence.
 For example, I am a software developer. => developer. am I software a
 */

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class WordSwitcher {

    public List<String> readFile(String path) {
        List<String> words = new LinkedList<>();
        try {
            File file = new File(path);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            while (line != null) {
                words.add(line);
                line = bufferedReader.readLine();

            }
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();

        }
        return words;
    }


    public void writeFile(String path, List<String> words) {
        try {
            File file = new File(path);
            FileWriter fileWriter = new FileWriter(file);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            List<String> switchedWords = switchWords(words);

            for (String switchedLine : switchedWords) {
                printWriter.println(switchedLine);
            }

            printWriter.close();

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    private List<String> switchWords(List<String> words) {
        List<String> swappedWords = new LinkedList<>();

        for (String word : words) {
            String[] lineArray = word.split(" ");

            String swappedWord = "";

            if (lineArray.length % 2 == 0) {
                for (int i = 0; i < lineArray.length; i = i + 2) {
                    swappedWord += lineArray[i + 1] + " " + lineArray[i] + " ";

                }
            } else {
                swappedWord += lineArray[lineArray.length - 1] + " ";
                for (int i = 0; i < lineArray.length - 1; i = i + 2) {
                    swappedWord += lineArray[i + 1] + " " + lineArray[i] + " ";
                }
            }
            swappedWords.add(swappedWord.substring(0, swappedWord.length() - 1));
        }
        return swappedWords;
    }

    public static void main(String[] args) {
        WordSwitcher wordSwitcher = new WordSwitcher();
        String readingPath = "/Users/kaiapalm/java_fundamental1/text_files/anothertextfile.txt";
        String writingPath = "/Users/kaiapalm/java_fundamental1/text_files/result.txt";
        List<String> words = wordSwitcher.readFile(readingPath);
        wordSwitcher.writeFile(writingPath, words);


    }


}
