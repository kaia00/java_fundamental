package lectures.fifteen.exercises;

import java.util.Scanner;

public class VowelsRemover {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String userInput = scanner.nextLine();

        VowelsRemover vowelsRemover = new VowelsRemover();
        vowelsRemover.removeVowels(userInput);

    }

    private void removeVowels(String input) {

        String fixedInput = input.replaceAll("[a,e,i,o,u,A,E,I,O,U]", "");
        System.out.println(fixedInput);

    }

}


/*

"(a|A|e|E|i|I|o|O|u|U)", "")


1 - remove vowels(a,e,i,o,u) from a given string and print to console.
For example;  Gokhan Polat => Gkhn Plt

*/

