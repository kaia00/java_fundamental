package lectures.fifteen.exercises;

public class MiddleElementOfAnArray {

    public static void main(String[] args) {
        MiddleElementOfAnArray middleElementOfAnArray = new MiddleElementOfAnArray();
        int[] myArray = {5, 6, 1, 4, 3, 2};

        middleElementOfAnArray.findMiddle(myArray);


    }

    private void findMiddle(int[] myArray) {

        int[] sortedArray = sortArray(myArray);

        if (sortedArray.length % 2 == 1) {
            System.out.println("Middle element : " + sortedArray[sortedArray.length / 2]);
        } else {
            System.out.println("Middle elements : " + sortedArray[sortedArray.length / 2 - 1] + " , " + sortedArray[sortedArray.length / 2]);
        }


    }

    private int[] sortArray(int[] myArray) {

        for (int i = 0; i < myArray.length; i++) {
            for (int j = i + 1; j < myArray.length; j++) {
                if (myArray[j] > myArray[i]) {
                    int temp = myArray[i];
                    myArray[i] = myArray[j];
                    myArray[j] = temp;
                }
            }
        }

        return myArray;

    }
}

/*
2 - find middle element of an array.
for example middle element of {2,16,15,4,12,65,3} = 12(when you sort the array 12 will be in middle)
  - if array element count is even, then there will be 2 middle element.
  {2,16,15,4,12,65,3,9} => 9,12
 */

