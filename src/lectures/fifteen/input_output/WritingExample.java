package lectures.fifteen.input_output;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class WritingExample {

    public void writeFile(String path) {
        try {
            File file = new File(path); // create or take File from path
            FileWriter fileWriter = new FileWriter(file); //bind File to Filewriter. if you put second parameter "true", it means you append
            // instead of overwriting the file!
            PrintWriter printWriter = new PrintWriter(fileWriter); //bind FileWriter to PrintWriter to print data


            printWriter.print("Kaia"); //print data in same line
            printWriter.print(" ");
            printWriter.println("Palm"); //print data and go next line
            printWriter.print("123455645");

            printWriter.close(); //to save data into file printWriter needs to be closed.

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    public static void main(String[] args) {
        WritingExample writingExample = new WritingExample();

        String path = "/Users/kaiapalm/java_fundamental1/text_files/testWritingFile.txt";  //
        // if testWritingFile does not exist, FileWriter will create automatically. if file exists, it will be overwritten.
        writingExample.writeFile(path);

    }
}