package lectures.eight;


import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome stranger");

        while (true) {

            System.out.println("Enter the sentence. If you are done write Done");
            String userInput = scanner.nextLine();

            if (userInput.equalsIgnoreCase("done")) {
                break;

            } else {
                System.out.println("Which order do you want to print? ");
                String inputOrder = scanner.nextLine();

                if (Integer.parseInt(inputOrder) <= 0) {
                    System.out.println("Can't be negative number");
                } else {
                    String[] splittedWords = userInput.split(" ");

                    int counter = 1;
                    boolean found = false;

                    for (String word : splittedWords) {
                        if (!word.isEmpty() && !word.equalsIgnoreCase(" ")) {
                            if (counter == Integer.parseInt(inputOrder)) {
                                System.out.println("Result Word: " + word);
                                found = true;
                                break;
                            } else {
                                counter++;
                            }
                        }
                    }
                    if (!found) {
                        System.out.println("We cannot find the word. Sentence is too short.");
                    }
                }
            }
        }
    }
}


