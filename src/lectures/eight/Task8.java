package lectures.eight;


import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome stanger");
        System.out.println("Enter number 1");
        double num1 = scanner.nextDouble();
        System.out.println("Enter number 2");
        double num2 = scanner.nextDouble();
        if (num1 * num2 < 1000) {
            System.out.println("Below 1000");
        } else if (num1 * num2 >= 1000 && num1 * num2 <= 2000) {
            System.out.println("Between 1000 and 2000 (included)");
        } else {
            System.out.println("Above 2000");
        }
        // thresholds? google it.
    }
}