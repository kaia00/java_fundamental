package lectures.eight;

import java.util.Scanner;


public class Task14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome stranger");

        if (scanner.hasNextInt() || scanner.hasNextDouble()) {
            System.out.println("It is a number");
        } else {
            System.out.println("It is not a number");
        }

    }

}


/*

Done! Let user test if works correctly.


Create class with main
|| scanner.hasNextDouble()

User enters word.

It will print if it is number, Otherwise print it is text.

 */