package lectures.eight;

public class Dog {


    private String dogName;
    private String dogSpecies;
    private int dogAge;

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public void setDogSpecies(String dogSpecies) {
        this.dogSpecies = dogSpecies;
    }

    public void setDogAge(int dogAge) {
        this.dogAge = dogAge;
    }

    void printDogAgeDecision() {
        if (dogSpecies.equalsIgnoreCase("Beagle")) {
            if (dogAge < 3) {
                System.out.println("Dog is young");
            } else if (dogAge > 6) {
                System.out.println("Dog is old");
            } else {
                System.out.println("Dog is adult");
            }
        } else if (dogSpecies.equalsIgnoreCase("Husky")) {
            if (dogAge < 4) {
                System.out.println("Dog is young");
            } else if (dogAge > 7) {
                System.out.println("Dog is old");
            } else {
                System.out.println("Dog is adult");
            }
        } else if (dogSpecies.equalsIgnoreCase("Chihuahua")) {
            if (dogAge < 3) {
                System.out.println("Dog is young");
            } else if (dogAge > 7) {
                System.out.println("Dog is old");
            } else {
                System.out.println("Dog is adult");
            }
        }
    }

  /*  public String getDogName() {
        return dogName;
    }

    public String getDogSpecies() {
        return dogSpecies;
    }

    public int getDogAge() {
        return dogAge;
    } */

    void printDogInformation() {


        System.out.println("Dog name: " + dogName + " Age: " + dogAge + " Species: " + dogSpecies);

    }
}


