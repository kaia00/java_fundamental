package lectures.eight;


import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Utilities utilities = new Utilities();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome stranger");

        while (true) {

            System.out.println("What operation do you want to do? Add/Remove/View/Search/Done ");
            String selection = scanner.next();

            if (selection.equalsIgnoreCase("Done")) {
                break;
            } else if (selection.equalsIgnoreCase("Add")) {
                utilities.add();

            } else if (selection.equalsIgnoreCase("View")) {
                utilities.view();

            } else if (selection.equalsIgnoreCase("Remove")) {
                utilities.remove();

            } else if (selection.equalsIgnoreCase("Search")) {

                utilities.search();
            }
        }
    }
}


