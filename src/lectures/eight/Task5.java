package lectures.eight;

import java.util.Scanner;


public class Task5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        while (true) {
            System.out.println("Please enter a number for factorial. -1 to exit the program. ");
            int number = scanner.nextInt();
            if (number == -1) {
                break;
            } else if (number < 0) {
                System.out.println("negtive nr. not acceptable");
            } else if (number == 0) {
                System.out.println("result : 1");
            } else {
                int result = 1;
                for (int i = number; i > 1; i--) {
                    result = result * i;
                }
                System.out.println("result " + result);
            }

        }
    }
}

    /*

    private int calculateFactorial(int number) {
        int factorial = number*(number - 1);
        return factorial;
    }


    public static void main(String[] args) {
        Task5 factorial = new Task5();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number for factorial. -1 is a special command to exit.");
        int userInput = scanner.nextInt();

        while (userInput == 0) {
            System.out.println(1);
        }
        while (userInput == -1) {
            return;
        }
        while (userInput < -1) {
            System.out.println("You cannot enter a negative number. Please choose another number. ");
            userInput = scanner.nextInt();
        }
        while (userInput >= 1) {
            System.out.println(factorial.calculateFactorial(userInput));
            return;

        }
        System.out.println("Please enter a number for factorial. -1 is a special command to exit.");


    }

}
*/



