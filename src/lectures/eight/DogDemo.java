package lectures.eight;

import java.util.Scanner;

public class DogDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Dog dog = new Dog();
        System.out.println("Welcome stranger");
        System.out.println("Enter your dog name");
        dog.setDogName(scanner.nextLine());
        while (true) {
            System.out.println("Enter your dog age");
            int age = scanner.nextInt();
            if (age < 0) {
                System.out.println("Dog age cannot be negative");
            } else {
                dog.setDogAge(age);
                break;
            }
        }

        while (true) {
            System.out.println("Enter your dog species");
            String dogSpecies = scanner.next();
            if (dogSpecies.equalsIgnoreCase("Chihuahua") || dogSpecies.equalsIgnoreCase("Husky") || dogSpecies.equalsIgnoreCase("Beagle")) {
                dog.setDogSpecies(dogSpecies);
                dog.printDogInformation();
                dog.printDogAgeDecision();
                break;

            } else {
                System.out.println("It is not Chihuahua or Husky or Beagle");
            }

        }
    }
}



/* create class Dog
Dog will have name,species, age fields

Dog will have a method which will print his age decision

for Beagle
if age is below 3 it is young
if between 3 to 6 it is adult
older than 6 then it is old

for Husky
if age is below 4 it is young
if between 4 to 7 it is adult
older than 7 then it is old

for Chihuahua
if age is below 3 it is young
if between 3 to 7 it is adult
older than 7 then it is old


create another method which will print all its information.



create another class it will have main.
Get name age and species. You cannot accept any species beside beagle husky and Chihuahua
age cannot be negative */