package lectures.eight;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Done! Let user test if works correctly.

public class Encryption {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome stranger");

        System.out.println("Which word you want to encrypt");
        String wordToEncrypt = scanner.nextLine();

        System.out.println("Which word you want to change the word to?");
        String replacement = scanner.nextLine();
        System.out.println("What is the sentence?");
        String inputSentence = scanner.nextLine();
        System.out.println(inputSentence);

      //  String fixedInput = inputSentence.replaceAll(wordToEncrypt, replacement);
      //  System.out.println(fixedInput);


        Pattern pattern = Pattern.compile((wordToEncrypt), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputSentence);
        String result = matcher.replaceAll(replacement);
        System.out.println(result);


    }


}

/*

Done! Let user test if works correctly.

Pattern p = Pattern.compile("Your word", Pattern.CASE_INSENSITIVE);
Matcher m = p.matcher("String containing words");
String result = m.replaceAll("Replacement word");

Esialgne idee:

 String fixedInput = input.replaceAll(regex, replacement);
    System.out.println(fixedInput);

Kuidas saada tööle James Bond - james bond

create class Encryption
containing main method.

ask user to get word which will be encrypted with the word which you want to encrypt to.


if you say encrypt apple to xxx

if user prints apple is nice
it will print xxx is nice
 */