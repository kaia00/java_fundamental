package lectures.eight;

import java.util.Scanner;

public class HeidiHW10 {
    public static void main(String[] args) {
        String[] myArray = new String[]{"", "", "", "", ""};
        int totalAmount = 0;


        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("What do you want to do: add/remove/view/search/done?");
            String command = scanner.nextLine();
            if (command.equalsIgnoreCase("done")) {
                break;
            } else if (command.equalsIgnoreCase("add")) {
                System.out.println("What do you want to add?");
                String incomingItem = scanner.nextLine();
                for (String existingItem : myArray) {
                    if (existingItem.equalsIgnoreCase(incomingItem)) {
                        System.out.println("This already exist");
                        break;
                    }
                }

                if (totalAmount == 5) {
                    System.out.println("Array is full");
                } else {
                    for (int count = 0; count < myArray.length; count++) {
                        if (myArray[count].equals("")) {
                            myArray[count] = incomingItem;
                            totalAmount++;
                            break;
                        }

                    }
                }

            }
        }

    }
}