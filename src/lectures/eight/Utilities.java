package lectures.eight;

import java.util.Scanner;

public class Utilities {

    String[] myArray = new String[]{"", "", "", "", ""};

    Scanner scanner = new Scanner(System.in);

    String newItem;

    void add() {

        boolean isFull = true;

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i].equals("")) {
                System.out.println("What item do you want to add?");
                newItem = scanner.next();
                for (int counter = 0; counter < myArray.length; counter++) {
                    if (myArray[counter].equalsIgnoreCase(newItem)) {
                        System.out.println("No need to add this, already exists at index " + counter);
                        return;
                    }
                }
                isFull = false;
                myArray[i] = newItem;
                System.out.println("Item added");
                break;
            }
        }
        if (isFull) {
            System.out.println("Array is full.");
        }
    }

    void view() {
        boolean isEmpty = true;
        for (int i = 0; i < myArray.length; i++) {
            if (!myArray[i].equals("")) {
                System.out.print(myArray[i] + " ");
                isEmpty = false;
            }
        }
        if (isEmpty) {
            System.out.println("Array is empty.");

        }
    }

    void remove() {
        System.out.println("What item do you want to remove?");
        newItem = scanner.next();
        boolean found = false;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i].equals(newItem)) {
                found = true;
                myArray[i] = "";
                System.out.println("Item removed");
            }
        }
        if (!found) {
            System.out.println("Item not found.");
        }
    }

    void search() {
        System.out.println("What item do you want to search?");
        newItem = scanner.next();
        boolean found = false;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i].equals(newItem)) {
                found = true;
                System.out.println("Item located at index " + i);
            }
        }
        if (!found) {
            System.out.println("Item not found.");
        }
    }
}

/* Now create Utilities Class. for the add, remove,search,view
Create an object and call from the method.

Result will be same as the Previous screenshot. */