package lectures.eight;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Welcome stranger.");
            System.out.println("Enter the sentence. If you are done write Done");
            String userInput = scanner.nextLine();

            if (userInput.equalsIgnoreCase("done")) {
                break;
            } else {
                String[] result = userInput.split(" ");
                System.out.println(result[0]);

            }
        }
    }
}

