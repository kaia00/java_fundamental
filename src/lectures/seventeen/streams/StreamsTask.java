package lectures.seventeen.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsTask {
    public static void main(String[] args) {

        List<String> fruits = Arrays.asList("apple", "banana", "kiwi", "pear", "orange", "strawberry", "pineapple", "mango", "watermelon", "coconut");
        List<String> vegetables = Arrays.asList("tomato", "potato", "carrot", "radish", "onion", "garlic", "cucumber", "zucchini", "white raddish", "leek");
        List<String> result = new ArrayList<>();
        //1.
        Stream<String> fruitStream = fruits.stream();
        Stream<String> vegetableStream = vegetables.stream();

        //find first:
        Optional<String> firstFruit = fruitStream.findFirst();


        System.out.println("First fruit is: " + firstFruit);
        Optional<String> firstVegetable = vegetableStream.findFirst();
        System.out.println("First vegetable is: " + firstVegetable);

        //2.Get any elements of both arraylist, concatenate and add to result array.

        Stream<String> findAnyFruit = fruits.stream();
        Stream<String> findAnyVegetable = vegetables.stream();

        Optional<String> anyFruit = findAnyFruit.findAny();
        Optional<String> anyVegetable = findAnyVegetable.findAny();
        result.add(anyFruit.get().concat(anyVegetable.get()));
        System.out.println(result);

//3. Add fruits which has a letter 'k', add vegetables that starts with letter 'B' to result array.

       // Stream<String> FruitsWithK = fruits.stream().filter(n -> n.contains("K")).collect(result);
      //  Stream<String> VeggieThatStartsWithB = vegetables.stream().filter(n->n.startsWith("B"));


    }


}

/*
Create two ArrrayLists of fruits and vegetables. The array values can be any and should contain atleast 10 in each array.
1. Get the first element of both arraylists using streams. If value present then addto a new array result else delete 6th element in both the arrays.
2. Get any elements of both arraylist, concatenate and add to result array.
3. Add fruits which has a letter 'k', add vegetables that starts with letter 'B' to result array.
4. Find the lengths of all fruits. Remove the fruits from fruits array whose lengths are less than 10.
5. Check if all the vegetables' lengths are greater than 0. If true, lowercase and concatenate all the vegetables with '-' using reduce function. Then add the string to result array.
6. Sort your result array with alphabet ascending using sorted method.
 */