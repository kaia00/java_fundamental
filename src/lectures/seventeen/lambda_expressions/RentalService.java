package lectures.seventeen.lambda_expressions;

import java.time.LocalDate;
import java.util.Date;

public class RentalService {

    private String firstname;
    private String lastname;
    private String model;
    private String place;
    private double price;
     Date from_Date;
     Date to_Date;


    public RentalService(String firstname, String lastname, String model, String place, double price, Date from_Date, Date to_Date) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.model = model;
        this.place = place;
        this.price = price;
        this.from_Date = from_Date;
        this.to_Date = to_Date;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getFrom_Date() {
        return from_Date;
    }

    public void setFrom_Date(Date from_Date) {
        this.from_Date = from_Date;
    }

    public Date getTo_Date() {
        return to_Date;
    }

    public void setTo_Date(Date to_Date) {
        this.to_Date = to_Date;
    }
}


/*
To create car rental services. Create a RentalService class with firstname, lastname, from_date, to_date, model, place and price.
 */