package lectures.seventeen.lambda_expressions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


public class RentalServiceDemo {


    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


     //   RentalService rentalService = new RentalService();

    }
}

/*
Task: To create car rental services. Create a RentalService class with firstname, lastname, from_date, to_date, model, place and price.
1. Create rental service object. Implement a predicate interface to find whether the rentalservice has ended already.
2. Use Lambda Expression to find whether rental service has ended already with price greater than 500.
3. Implement a Function Reference with lambda expression to capitalize and concatenate the firstname and lastname.
4. Add a method to RentalService class to find whether the to_date is not Today.
5. Get a random Price and update the price of the rental service using Supplier and Consumer.
6. Use a lambda operation to give 20% on given rental price.
 */